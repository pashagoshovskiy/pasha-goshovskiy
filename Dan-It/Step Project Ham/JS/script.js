$(document).ready(() => {
    $('.services-nav-item').click(function () {
        let takeNavId =$(this).attr('data-navId');
        $('.services-nav-item').removeClass('services-nav-item-active');
        $('.services-info-item').removeClass('services-info-item-active');

        $(this).addClass('services-nav-item-active');
        $('[data-infoId =' + takeNavId + ']').addClass('services-info-item-active');
    })
});

/*Amazing Work - tabs*/
let tabsFilter = $('li.amazing-nav-item');

tabsFilter.click((e)=>{
    let targetTab = $(e.target);
    let imageBlock = $('div.amazing-item');

    if (targetTab.attr('data-type') === 'all'){
        imageBlock.removeClass('hidden');
    } else {
        imageBlock.removeClass('hidden')
        $.each(imageBlock, (index,img) => {
            if (targetTab.attr('data-type') !== $(img).attr('data-type')){
                $(imageBlock[index]).addClass('hidden')
            }
        })
    }
});

/*Amazing Work - loading btn*/
$('.load-more-btn').on('click', function (){
    $('.amazing-item:hidden').slice(0,12).show();

    if($('.amazing-item:hidden').length ==0){
        $('.load-more-btn').fadeOut();
    }
})
/*Slider*/
$('.people-info-container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider'
});

$('.slider').slick({
    arrows: true,
    adaptiveHeight: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.people-info-container',
    centerMode: true,
    focusOnSelect: true,
    centerPadding: 0
});



