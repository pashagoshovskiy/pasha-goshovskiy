//Технические требования:
//
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя,
// все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат
// выполнения функции.
//
//
// Необязательное задание продвинутой сложности:
//
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

const getFirstName = prompt('Enter first name:');
const getSecondName = prompt('Enter second name:');

function createNewUser(userName, userLastName) {
    const newUser = {
        firstName: userName,
        lastName: userLastName,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    }
    console.log(newUser.getLogin());
}

const newUser = createNewUser(getFirstName, getSecondName);
console.log(newUser);
