document.addEventListener('DOMContentLoaded', evt => {
    let firstField = document.querySelector('.first-field');
    let secondField = document.querySelector('.second-field');
    let firstEye = document.getElementById('firstOpenedEye');
    let secondEye = document.getElementById('secondOpenedEye');
    let submitButton = document.querySelector('.btn');

    const wrongPassword = document.createElement('div');
    wrongPassword.className = 'wrong-password-text';
    document.querySelector('.password-form').append(wrongPassword);

    const comparePassword = () => {
        if (firstField.value === secondField.value) {
            wrongPassword.innerText = '';
            alert('You are welcome');
        } else {
            wrongPassword.innerText = 'Нужно ввести одинаковые значения';
        }
    }

    submitButton.addEventListener('click', comparePassword);

    firstEye.addEventListener('click', () => {
        if (firstField.type === 'password') {
            firstField.type = 'text';
            firstEye.className = 'fas fa-eye-slash icon-password';
        } else {
            firstField.type = 'password';
            firstEye.className = 'fas fa-eye icon-password';
        }
    })
    secondEye.addEventListener('click', () => {
        if (secondField.type === 'password') {
            secondField.type = 'text';
            secondEye.className = 'fas fa-eye-slash icon-password';
        } else {
            secondField.type = 'password';
            secondEye.className = 'fas fa-eye icon-password';
        }
    })

});

