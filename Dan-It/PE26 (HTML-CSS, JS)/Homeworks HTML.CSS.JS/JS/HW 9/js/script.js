document.addEventListener('DOMContentLoaded', evt => {

    let tabsMenu = document.querySelector('.tabs');
    let tabsItems = document.querySelectorAll('.tabs-title');
    let tabsContent = document.querySelectorAll('.tab-item');

    tabsMenu.addEventListener('click', e => {
        let target = e.target;

        tabsItems.forEach((i) => {
            i.classList.remove('active');
        })
        target.classList.add('active');

        tabsContent.forEach((elem) => {
            if (elem.dataset.tabContent === target.dataset.tabHead) {
                elem.classList.add('active')
            } else {
                elem.classList.remove('active')
            }
        })
    })


})
