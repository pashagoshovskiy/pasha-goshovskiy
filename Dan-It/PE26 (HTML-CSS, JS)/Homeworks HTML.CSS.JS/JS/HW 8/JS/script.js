document.addEventListener('DOMContentLoaded', () => {
    function focusFunction() {
        document.getElementById('priceInput').style.border = '5px solid green';
        priceValue.remove();
    }

    function blurFunction() {
        document.getElementById('priceInput').style.border = '';
        priceValue();
    }

    const priceInput = document.createElement('input');
    priceInput.setAttribute('type', 'number');
    priceInput.setAttribute('id', 'priceInput');

    priceInput.addEventListener('focus', focusFunction);
    priceInput.addEventListener('blur', blurFunction);

    const resetButton = document.createElement('button');
    resetButton.setAttribute('type', 'reset');
    resetButton.setAttribute('id', "resetBtn");
    resetButton.innerHTML = 'X';

    resetButton.addEventListener('click',() => {
        priceInput.value = '';
        correctSpan.remove();
        incorrectSpan.remove();
        resetButton.remove();
    })

    const correctSpan = document.createElement('span');

    const incorrectSpan = document.createElement('span');

    document.body.prepend(priceInput);



    let priceValue = () => {
        if (parseInt(priceInput.value) > 0) {
            incorrectSpan.remove();
            correctSpan.textContent = `Текущая цена: ${priceInput.value}`
            priceInput.style.color = 'green';
            document.body.prepend(correctSpan,resetButton);
        } else {
            priceInput.style.border = '5px solid red';
            correctSpan.remove();
            incorrectSpan.textContent = 'Please enter correct price';
            priceInput.style.color = '';
            return document.body.prepend(incorrectSpan,resetButton)
        }
    }
});

