const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown',e => {
    buttons.forEach((elem)=>{
        if (elem.innerText.toLowerCase() === e.key.toLowerCase()){
            elem.style.backgroundColor = 'blue';
        } else {
            elem.style.backgroundColor = 'black';
        }
    })
})