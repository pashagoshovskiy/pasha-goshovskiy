/*Технические требования:

    Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

    При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
    Создать метод getAge() который будет возвращать сколько пользователю лет.
    Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вывести в консоль результат
*/

const getFirstName = prompt('Enter first name');
const getLastName = prompt('Enter second name');
const date = prompt ('Enter your birthday - dd.mm.yyyy');
function createNewUser(firstName, lastName, date) {

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: new Date(date.slice(6,10)+ '-' + date.slice(3,5)+ '-' + date.slice(0,2)),
        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge() {
            return Math.round(Date.now()/1000/3600/24/365 - this.birthday.getTime()/1000/3600/24/365);
        },
        getPassword(){
            return this.firstName.charAt(0).toUpperCase()+this.lastName.toLowerCase()+this.birthday.toDateString().slice(11,15)
        }
    }
    return newUser;

}
const newUser = createNewUser(getFirstName,getLastName,date);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);