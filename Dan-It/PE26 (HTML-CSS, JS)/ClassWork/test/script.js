// const musician = {
//     name: "Виктор",
//     "last name": "Цой",
//     status: "Жив!"
// };
//
// for(let key in musician) { // сохраняем в переменную key на каждой итерации цикла имя следующего свойства объекта
//     console.log(musician[key]); // поскольку в переменной key хранится название свойства объекта, то мы может к нему обратиться через имяОбъекта[имяПеременной]
// }
function first(y){
    console.log(1);
    y();
}
function second(a,b){
    console.log(a*b);
}
first(() => second(5,6));
// second(4,7);