/* TASK - 5 add the functionality for your mail client from the previous lesson
* Place the mailStorage in the localStorage.
* When user creates an email - user localStorage API to have the access to all the letter, add new one to mailStorage and replace the current value of mailStorage in the localStorage.
* Same thing with deleting letters.
* */
const allEmails = [
    {
        subject: "Hello world",
        from: "gogidoe@somemail.nothing",
        to: "lolabola@ui.ux",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "How could you?!",
        from: "ladyboss@somemail.nothing",
        to: "ingeneer@nomail.here",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    },
    {
        subject: "Acces denied",
        from: "info@cornhub.com",
        to: "gogidoe@somemail.nothing",
        text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
    }
];

const emailsFromStorage = localStorage.getItem('emails');

if (emailsFromStorage === null) {
    localStorage.setItem('emails', JSON.stringify(allEmails))
};

const allEmailsFromLocalStorage = JSON.parse(emailsFromStorage);
console.log(allEmailsFromLocalStorage);



const createEmailElem = (emailObj) => {
    const emailItem = document.createElement('div');
    emailItem.className = "email-item";

    emailItem.insertAdjacentHTML('beforeend', `
      <h4 class="email-subject">${emailObj.subject}</h4>
  
      <div class="email-subtext-wrapper">
        <a href="mailto:gogi@go.com" class="email-subtext email-from">${emailObj.from}</a>
        <a href="mailto:escobar@pablo.coco" class="email-subtext email-to">${emailObj.to}</a>
      </div>
  
      <p class="email-text" hidden>${emailObj.text}</p>
  `);

    return emailItem;
};

const emailsContainer = document.querySelector('.emails');
const fragment = document.createDocumentFragment();

allEmailsFromLocalStorage.forEach((singleEmail) => {
    const emailElement = createEmailElem(singleEmail);
    fragment.append(emailElement);
});

setTimeout(() => {
    document.querySelector('.preloader').remove();
    emailsContainer.append(fragment);
}, 2000);


const newEmail = document.querySelector('.create-email');

newEmail.addEventListener('click', () => {
emailsContainer.insertAdjacentHTML('beforebegin', `<form>
<input type = 'text' class = 'subject' placeholder= 'subject'>
<input type = 'text' class = 'mailForm' placeholder = 'mailForm'>
<input type = 'text' class = 'mailTo' placeholder = 'malTo'>
</form>`);
});


console.dir(document.getElementsByTagName("input"));
