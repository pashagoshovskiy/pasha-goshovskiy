/* TASK - 1
* Write showMsg(msgText, time) function, where:
*   msgText - text of the message that will be shown
*   time - number of milliseconds of delay to show the message
* */

const showMsg = (msgText, time) => {
    setTimeout(() => document.write(msgText), time,)
};
showMsg('Here is your bear', 3000);
