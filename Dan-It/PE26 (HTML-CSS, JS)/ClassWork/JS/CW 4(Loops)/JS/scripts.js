// /*while*/
// let iterationCount = 0;
// while (iterationCount < 50) {
// iterationCount++
// }
// /*for*/
// for (let i = 0; i < 50; i++) {
//
// }
//
// const user = {
//     name : "Gogi",
//     age : 60,
//     favouriteNumbers: [10,13,4,0.7],
//     wife: null,
//     pets: undefined
// };
// /*for IN*/
// for (let key in user) {
//     console.log(key);
// }
// /* for OF*/
// for (let opop of user.favouriteNumbers) {
//     console.log(opop);
// }
//
// for (let i = 0; i < user.favouriteNumbers.length ; i++) {
//     console.log(user.favouriteNumbers[i]);
// }