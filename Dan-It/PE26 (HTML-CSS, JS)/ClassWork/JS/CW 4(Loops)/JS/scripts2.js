/* TASK - 1
* User needs to enter name and age.
* If the user entered incorrect data, we need to ask him again and again,
* UNTIL the name and age will be entered correctly.
* Incorrect data is: the number instead of name, letter symbols instead of age
* */
//
// let name = prompt('Enter your name:');
//
// while (!isNaN(name)){
//     name = prompt('Enter your name again (incorrect name):');
// }
//
// let age = +prompt('Enter your age:');
//
// while (isNaN(age) || age === null){
//     age = prompt('Enter your age again (incorrect age): ');
// }

/*---------------------------------------------------------------*/
/* TASK - 2
* Show first 147 odd numbers starting from 1.
*   ADVANCED COMPLEXITY - do not show numbers witch can be divided by 5 without residue.
* */
// let counter = 0;
// for (let i = 1; i < 294; i+=2) {
//     console.log('number ->',i, "\n" + ++counter + 'odd number');
// }
/*---------------------------------------------------------------*/
/* TASK - 3
* Create checking for correctness of data entry.
* User should write two numbers and the symbol of the operation.
* Ask user to enter data again and again UNTIL it'll be entered correctly.
* Each of two numbers should be integer, only operations from the list are allowed:
*   * - multiply
*   + - addition
*   - - subtraction
*   / - dividing
* */
// let firstNum = +prompt("Enter first number:");
// while (isNaN(firstNum) || firstNum !== parseInt(firstNum) || firstNum === null){
//     firstNum = +prompt("Enter first number:");
// }
// let secondNum = +prompt("Enter second number:");
// while (isNaN(secondNum) || secondNum !== parseInt(secondNum) || secondNum === null){
//     secondNum = +prompt("Enter second number:");
// }
// let operation = prompt("Enter operation:");
// while (operation !== "*" && operation !== "+" && operation !== "-" && operation !== "/" ) {
//     operation = prompt("Enter operation:");
// }
/*---------------------------------------------------------------*/
/* TASK - 4 calculator
* Ask user to enter two numbers and operation.
* Check for correctness of data entry from the previous task.
* Show the message with the result of calculations
*
* ADVANCED_COMPLEXITY:
*     - keep in memory the result of the last operation. If one of two numbers was entered as `PREV_OP`
*      replace the result of last operation instead of thin number.
*     - add more operations to the list -> raising to the power, taking the root of number 1 of the power of number 2.
*     - create a function, which will ha second number, operation.
*/
do{
let firstNum = prompt("Enter first number:");
while (isNaN(firstNum) || +firstNum !== parseInt(firstNum) || firstNum === null){
    firstNum = prompt("Enter first number:");
}
let secondNum = prompt("Enter second number:");
while (isNaN(secondNum) || +secondNum !== parseInt(secondNum) || secondNum === null){
    secondNum = prompt("Enter second number:");
}
let operation = prompt("Enter operation:");
while (operation !== "*" && operation !== "+" && operation !== "-" && operation !== "/" ) {
    operation = prompt("Enter operation:");
}
    let result;
switch (operation){
    case "*":
        result = +firstNum * +secondNum;
        alert(result)
        break;
    case "+":
        result = +firstNum + +secondNum;
        alert(result)
        break;
    case "-":
        result = +firstNum - +secondNum;
        alert(result)
        break;
    case "/":
        result = +firstNum / +secondNum;
        alert(result)
        break;
    default: alert('Ты рагуль!')
}
if (firstNum === `PREV_OP` || secondNum === `PREV_OP`){
alert("helo")
}
} while (confirm('would you like to continue?'));
/*---------------------------------------------------------------*/
/* TASK - 5
// * You have an object like this:*/
// let user = {
//     name: 'Kenny',
//     surname: 'Doe',
//     birthDay: '01.12.1988',
//     description: 'best men ever',
//     pets: {
//         name: 'Bob',
//         age: 14,
//         status: 'dead'
//     }
// }
/* The task is show all of the properties of this object in console like this:
* PROPERTY_NAME - PROPERTY_VALUE.
* 	TASK-5.1
* 	Same thins with an Array ['Gogi', 'Goga', 'Gogo', 'Gugu', 'Gunigugu', 'Guguber', 'Gigi'].
* 	Display all of the elements of this array in console.
*
 */
/*---------------------------------------------------------------*/
// 5.0
// for (let value in user){
//     console.log(value + " - " , user[value]);
// }
/*---------------------------------------------------------------*/
// 5.1
// let arr = ['Gogi', 'Goga', 'Gogo', 'Gugu', 'Gunigugu', 'Guguber', 'Gigi'];
// for (let result of arr) {
//     console.log(result);
// }
