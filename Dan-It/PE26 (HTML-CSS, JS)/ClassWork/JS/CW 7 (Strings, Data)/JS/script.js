/*TASK 1
* Write a function customCharAt(string,index)
* string - source string
* index - the index of the perticular character of the string that we need
* Return value: the character itself, that is placed on the specific index
* */
// function customCharAt(string, index){
//     return string[index];
// }
// let stringName = "asd";
// console.log(customCharAt(stringName,1));

/*TASK 2
* Ask a string from the user and turn all of the odd characters of this string in to UPPERCASE
*/
// function oddToUpperCase(string) {
//     let newString = '';
//     for (let i = 0; i < string.length; i++) {
//         if (i % 2 === 0){
//             newString += string[i].toUpperCase();
//         } else {
//             newString += string[i];
//         }
//     }
//     return newString;
// }
// let strName = prompt ('Enter smth:');
//
// console.log(oddToUpperCase(strName));
// function oddToUpperCase(string) {
//     const words = string.split(' ');
//     let newString = '';
//     for (let singleWord of words) {
//         singleWord = singleWord.trim();
//         let newWord = '';
//         for (let i = 0; i < singleWord.length; i++) {
//             if (i % 2 === 0){
//                 newWord += singleWord[i].toUpperCase();
//             } else {
//                 newWord += string[i];
//             }
//         }
//         newString += ' ' + newWord
//     }
//
//     return newString;
// }
// let strName = prompt ('Enter smth:');
//
// console.log(oddToUpperCase(strName));

/* TASK 3
* Create a function cutMexLength(string, maxlength)
* You need to cun the exact number of characters(maxLength) from source string.
* Return value:
*       if the number of the characters in source string is bigger then maxLength - the string that has been cut
*       if the number of the characters in source string is smaller then maxLength - the source string itself
*/

// function cutMexLength(string, maxlength){
//     return string.length > maxlength
//     ? string.slice (0, maxlength) + '...'
//     : string;
// }
//
// console.log(cutMexLength('qwertyu', 6));
//************************************************
// const today = new Date();
// const todayNow = Date.now();
//
// console.log(today);
// console.log(typeof today);
// console.log(todayNow);
// console.log(typeof todayNow);

// today.setFullYear('1970');
// console.log(today)

/* TASK 4
* Create a function getDayAgo(numberOfDays)
* Its needs to return a name of the weekday, that was numberOfDays days before.
*/
// function getDayAgo(numberOfDays) {
//     let week = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
//     let date = new Date();
//     date.setDate(date.getDate() - numberOfDays);
//     let dayAgo = date.getDay();
//     return week[dayAgo];
// }
//
// console.log(getDayAgo(3));

/*TASK 5
* Create a function that takes string with date 'DD/MM/YYYY' as an argument.
* Return value: number of weekday of the first day if this month
* Create the same function for getting the number of the last weekday of the month
* */

function firstMonthDay (dateString) {
    let dateArr = dateString.split('/');
    let date = new Date(`${dateArr[1]}/${dateArr[0]}/${dateArr[2]}`);
    date.setDate(1);
    return date.getDay()
}

console.log(firstMonthDay('16/09/2020'));

/* TASK 6
* You have an object of a storage
* Create a method getAmount(goodsList), that takes a string with the list of goods.
* Return value: a modified string, that has next format for each good - 'goodName: amountOfThisGoodInStorage'
*
* In case if there is no good with this name in storage, place 'not found' instead of amount.
*
* Your program doesn't need to be case sensitive.
* It means that the 'pIneAPple' needs to be the same as 'pineapple'
*
* 	ADVANCED COMPLEXITY:
*           add the grouping by categories to the storage with any representation.
*           add 2-3 categories of goods with 4-5 goond in each category
*           add the third argument in the function. If it exists - search in particular category, if there is no third argument - search everywhere
*/
const Storage = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    blackTea: 13,
    cheese: 0,
};

Storage.getAmount = function (goodlist) {
    const goods = goodlist.split(', ');
    let resultString = '';

    for (let goodName of goods) {
        resultString+=`${goodName}: ${this[goodName] >= 0 ? this[goodName] : 'not found'} \n`;
        console.log(Storage[goodName]);
    }
    return resultString;
};

console.log(Storage.getAmount('milk, coffee, cheese, asd'));