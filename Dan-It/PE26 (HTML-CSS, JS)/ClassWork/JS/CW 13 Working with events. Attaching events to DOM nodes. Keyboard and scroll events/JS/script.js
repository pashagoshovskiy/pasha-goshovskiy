// document.oncontextmenu = e => {
//     e.preventDefault();
// }
// Блокирует при нажатии правой кнопкой

// console.log(document.getElementsByTagName("script"));
// document.onkeypress = e => console.log(`keypress of - ${e.code}`);
// document.onkeyup = e => console.log(`keyup  of - ${e.code}`);
// document.onkeydown = e => console.log(`keydown of - ${e.code}`);

// document.addEventListener('DOMContentLoaded', e => {
//     console.log('DOMContentLoaded');
// });
// window.addEventListener('load', e => {
//     console.log('loaded');
// });
// window.addEventListener('unload', e => {
//     console.log('unloaded');
// });

//  * Task - 01
//  *
//  * Create h1 element on a page using JavaScript. Place it on the page.
//  * After any kay on the page was pressed, print the name of this key as a text content of the h1 element.
//  * */

// const createTitle = () =>{
//     const titleRef = document.createElement('h1');
//     titleRef.classList.add('key-marker');
//     window.addEventListener('keydown', (e) => {
//         titleRef.textContent += e.key
//     });
//     document.querySelector('.container').prepend(titleRef);
// };
// createTitle();

//  * Task - 02
//  *
//  * Create p element that will represents how many pixels user had scrolled on the page from the top of it.
//  * */

// const paragraph = document.createElement('p')
//
// paragraph.classList.add('scroll-monitor');
//
// // document.addEventListener('scroll', (e) => {
// //   paragraph.textContent = window.pageYOffset;
// // });
//
// /* ADVANCED */
// document.addEventListener('scroll', (e) => {
//     const all = document.documentElement.scrollHeight - document.documentElement.clientHeight;
//
//     const currentScroll = window.pageYOffset || document.documentElement.scrollTop;
//
//     paragraph.textContent = `${Math.floor((currentScroll / all) * 100)}%`;
// });
//
// document.body.prepend(paragraph);

/*
 * Task - 03
 *
 * Block default behaviour of a scroll event, and replace it's functionality.
 *
 * To do so - create a circle using JS. Place it on the page.
 * Make this circle move right-to-left when user scrolls.
*/

//
// window.addEventListener('scroll', e =>{
// const image = document.getElementById("moveMe");
// image.style.left = `${window.pageYOffset}px`;
// });

/*
 * Task - 05
 *
 * Add a couple of sections to your page by writing HTML code.
 * To make creation easier you can just set the height and different background color for each section.
 *
 * This sections should take 5-7 screens to scroll.
 *
 * Add animation to each section. The list of animations you already have in CSS file attached to the page.
 *
 * The task is to make all of the sections appear only when user scrolls down to the beginning of the section.
 * */

// const makeAnimatedByScroll = (selector) => {
//     const elToFade = document.querySelector(selector);
//     const sectionOffset = elToFade.offsetTop;
//     window.addEventListener('scroll', () => {
//         console.log(window.pageYOffset >= sectionOffset);
//         if (window.pageYOffset >= sectionOffset){
//             elToFade.style.animationPlayState = 'running';
//         } else {
//             elToFade.style.animationPlayState = 'pause';
//         }
//
//     })
//     console.dir(elToFade);
// };
// makeAnimatedByScroll('.block-third')

/*
 * Task - 04
 *
 * Recreate a pack-man from the CSS Animation lesson, but using JS. Place it on the page.
 * Make user controls direction of pack-man movement by pressing W, S, A, D keys.
 *
 *  W - pack-man moves 20px to top from it's current position;
 *  S - pack-man moves 20px to bottom from it's current position;
 *  A - pack-man moves 20px to the left from it's current position;
 *  D - pack-man moves 20px to the right from it's current position;
 */

// const packMan = document.querySelector('.pack-man');
// console.dir(packMan);
// document.addEventListener('keydown', e => {
//     switch (e.key){
//         case 'd':
//             packMan.style.left = packMan.offsetLeft + 20 + 'px';
//             packMan.style.transform = 'rotate(0deg)';
//             break;
//         case 'a':
//             packMan.style.left = packMan.offsetLeft - 20 + 'px';
//             packMan.style.transform = 'rotate(0deg) scale(-1, 1)';
//             break;
//         case 'w':
//             packMan.style.top = packMan.offsetTop - 20 + 'px';
//             packMan.style.transform = 'rotate(90deg) scale(-1, 1)';
//
//             break;
//         case 's':
//             packMan.style.top = packMan.offsetTop + 20 + 'px';
//             packMan.style.transform = 'rotate(-90deg) scale(-1, 1)';
//             break;
//     }
// });