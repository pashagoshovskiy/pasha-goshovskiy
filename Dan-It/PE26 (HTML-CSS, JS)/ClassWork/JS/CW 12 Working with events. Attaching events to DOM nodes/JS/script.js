// const myFunc = (callBack) => {
//     callBack;
// }
// myFunc(() => console.log('gogi'));

/*TASK - 1
* Show alert with message 'This is click', after the click on the 'Click me' button.
*/

//
// const clickBtn = document.getElementById('click-me');
// clickBtn.addEventListener('click', () => alert('This is click'));

/*TASK - 2
* Show alert with message 'This is mouseover', after the hover on the 'Click me' button.
*/

// const clickBtn = document.getElementById('click-me');
// clickBtn.addEventListener('mouseover', () => alert('This is mouseover'));

/*TASK - 3
* Create a function, that will be changing the background color of the 100px square randomly,
* by the clicking on it. Every color should be random, transparent and white are not included in the list of colors.
 */

//
// const randomNum = (maxNumber) => {
//     return parseInt(Math.random() * maxNumber);
// }
//
// const randomColor = () => `rgb(${randomNum(255)}, ${randomNum(255)}, ${randomNum(255)})`
//
// const createSquare = (size) => {
//     let square = document.createElement('div');
//
//     square.style.width = size;
//     square.style.height = size;
//     square.style.backgroundColor = randomColor();
//
//     square.addEventListener('click', () => {
//         square.style.backgroundColor = randomColor();
//     });
//     document.body.prepend(square);
// };
// createSquare('100px');

/*TASK - 4
* There is a button 'Change color' next ot the 100px square. Create a function, that will change the color of the square randomly, by the clicking on the button, not on the square.
*/

// const randomNum = (maxNumber) => {
//     return parseInt(Math.random() * maxNumber);
// }
//
// const randomColor = () => `rgb(${randomNum(255)},${randomNum(255)},${randomNum(255)})`;
//
// const createSquare = (size) => {
//     let square = document.createElement('div');
//     square.style.width = size;
//     square.style.height = size;
//     square.style.backgroundColor = randomColor();
//
//     const btn = document.createElement('button', );
//     btn.textContent = 'click me'
//     btn.addEventListener('click', () => {
//         square.style.backgroundColor = randomColor();
//     });
//
//     document.body.prepend(square,btn);
// };
// createSquare('100px');

/*ALTERNATIVE*/

// const randomNum = (maxNumber) => {
//     return parseInt(Math.random() * maxNumber);
// }
//
// const randomColor = () => `rgb(${randomNum(255)},${randomNum(255)},${randomNum(255)})`;
//
// const createSquare = (size) => {
//     let square = document.createElement('div');
//     square.style.width = size;
//     square.style.height = size;
//     square.style.backgroundColor = randomColor();
//
//     const btn = document.createElement('button', );
//     btn.textContent = 'click me'
//     btn.addEventListener('click', (e) => {
//         e.target.previousSibling.style.backgroundColor = randomColor();
//     });
//
//     document.body.prepend(square,btn);
// };
// createSquare('100px');

/*TASK - 5
* There is an input next to the 100px square.
* Only HEX of the color can be entered in this input. Next to this input - 'Ok' btn is placed and its inactive by default.
* Create a function that will be changing the color.
* After hex is entered 'Ok' button will start to be active and you can press it.
* After pressing the 'Ok' button color needs to be changed.
* Рядом с блоком размером 100рх на 100рх располагается поле
* */

// const customColor = () => {
//     let input = document.createElement('input');
//     let square = document.createElement('div');
//     let btn = document.createElement('button');
//
//     btn.textContent = 'Ok';
//     square.style.width = '100px';
//     square.style.height = '100px';
//     square.style.backgroundColor = 'blue';
//
//     btn.addEventListener('click', () => {
//         square.style.backgroundColor = input.value;
//     });
//     document.body.prepend(input, btn, square);
// }
// customColor();

/*TASK - 6
* Create red circle 80px radius.
* Create a function that will be responsible for sticking this circle up to mouse cursor.
* It means that the center of the circle should be placed on the cursor always, even when the mouse moves.
* Add delay to create an illusion of catch-ups.
* */

//
// const circle = document.createElement('div');
// circle.style.cssText = `
// border-radius: 50%;
// background: red;
// width: 100px;
// height: 100px;
// position: absolute;
// top: 0;
// left: 0;
// // transition: 0.3s left, 0.3s top;
// `;
// document.body.prepend(circle);
//
// document.body.addEventListener('mousemove',(e) => {
//     const cursorTop = e.clientY;
//     const cursorLeft = e.clientX;
//
//     circle.style.top = `${cursorTop - 50}px`;
//     circle.style.left = `${cursorLeft - 50}px`;
// });

