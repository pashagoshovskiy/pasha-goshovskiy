/* TASK - 1:
* Create a square.
* Ask the user about the size and background color of the square.
* Create an element in JS
* Ask the size.
* Ask the background color.
* Add the styles to the element in JS
* Place the element BEFORE fist script tag in your page
* */

// let userSize = prompt('Enter size');
// let userColor = prompt('Enter color');
// const container = document.createElement('div');
// // container.style.width = `${userSize}px`;
// // container.style.height = `${userSize}px`;
// // container.style.backgroundColor = userColor;
//
// //ALTERNATIVE
// container.style.cssText = `
// width: ${userSize}px;
// height: ${userSize}px;
// background: ${userColor};
//  `;
//
// document.body.prepend(container);
// console.log(container);

/* TASK - 2
* Create two squares, using the same way that described in previous task.
* But this squares will have a different background colors.
* Create two elements in JS
* Ask the user about the size for both of this squares
* Ask the user about fist background color
* Ask the user about second background color
* Add the styles to both squares
* Place both squares BEFORE the first element with the script tag
* */

// let squareSize = prompt('Enter size');
// let userBg1 = prompt('Enter first square bg');
// let userBg2 = prompt('Enter second square bg');
//
// const createSquares = (size, bg1, bg2) => {
//     let customSquare = document.createElement('div');
//     customSquare.style.width = size + 'px';
//     customSquare.style.height = size + 'px';
//     customSquare.style.backgroundColor = bg1;
//     let customSquare2 = document.createElement('div');
//     customSquare2.style.width = size + 'px';
//     customSquare2.style.height = size + 'px';
//     customSquare2.style.backgroundColor = bg2;
//     document.body.prepend(customSquare,customSquare2);
// };
// createSquares(squareSize,userBg1,userBg2);

/* TASK - 3
* Rewrite TASK-2.
* Ask user the amount of rectangles that will be created and the background color for all of them.
* Create a CSS class with the basic properties: width: 200px; height: 200px; margin: 5px
*
* Ask the number of the rectangles
* Ask the background color
* Create a loop, where inside the each iteration:
*   create a rectangle
*   add class created in CSS
*   add background color
*   append the element on the page BEFORE the first element with the tag script
*/

// const createElem = (color) => {
//     const rectangle = document.createElement('div');
//     rectangle.classList.add('rectangles');
//     rectangle.style.backgroundColor = color;
//     document.body.prepend(rectangle);
// }
// const createRectangle = (number, color) => {
//     for (let i = 0; i < number; i++) {
//         createElem(color);
//     }
// };
// const num = +prompt('Enter number');
// const color = prompt('Enter color');
//
// createRectangle(num, color);
//

// const createElement = (color, classes) =>{
//     const rectangle = document.createElement("div");
//     rectangle.classList.add(classes);
//     rectangle.style.backgroundColor = color;
//     document.body.prepend(rectangle)
// };
//
// const createSq = (num, color) =>{
//     for (let i = 0; i < num; i++) {
//         createElement(color, 'rectangles');
//     }
// };
//
// const num = +prompt("Enter number");
// const color = prompt("Enter color");
//
// createSq(num, color);

/* TASK - 4:
* Create a chess desk. Div container for it has to ba on the HTML by default and has id='chess'.
* Create a class for basic styles of the desk cells: display:inline-block; width:150pxl height:150px; margin:0;
* User set the size of the desk and the two colors for it.
* Ask the size of the desk - it has to be a even number, so add the correctness checking
* Change the size of the desk container, that will fit the amount of cells.
* Ask the color for "white" cells
* Ask the color for "black" cells - it can't be the same, add the checking for this.
* Create a document fragment
* Create a loop, inside the each iteration you will:
*   create a rectangle
*   add the class with basic js properties
*   add the correct background color for this particular cell
*   append this rectangle into a document fragment
* After the loop:
*   append the document fragment as a child element inside the chess desk container
* */

