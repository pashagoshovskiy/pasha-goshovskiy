// const user = {
//     name: 'Eran',
//     age: 13,
//     pets:[
//         {name: 'Ricci', type: 'squirrel', age: 2, status: 'alive'},
//         {name: 'Vic', type: 'worm', age: 0.8, status: 'lost'},
//         {name: 'John', type: 'dog', age: 8, status: 'dead'}
//     ]
// };
//
// let propertyName = 'name';
//
//
// console.log("user's name -", user.name);
// console.log("user 'name' -", user[propertyName]);
//
// console.log("user's age -", user.age);
// console.log("user's name -", user.pets);
// console.log("user's firstname -", user.pets[0]);

/* TASK - 1
* Create a function, that takes two arguments - name and age of the user.
* Return value: an object with two keys: name, age.
* value of each key is a value of the function arguments.
* */

// function createUser (userName, userAge) {
//     const user = {
//         name: userName,
//         age: userAge
//     };
//
//     return user;
// }
//
// const newUser = createUser('Serhio', 33);
// console.log(newUser);

// //Alternative solutions
// function createUser (userName, userAge) {
//     return {
//         name: userName,
//         age: userAge
//     }
// }
// function createUser (userName, userAge) {
//     return {
//         userName,
//         userAge
//     }
// }

/* TASK - 2
* Add some functionality to the previous task
* Write an object method that will be increasing the age value by 1
* and add it to the resulting object.
* That means that inside the resulting object will be the key named 'increaseAge',
* the value of this key will be the functions, that takes a value of the key 'age'
* from this object and increase it.
* */

// function createUser (userName, userAge) {
//     return {
//         name: userName,
//         age: userAge,
//         increaseAge: function () {
//             this.age++;
//         }
//     };
// }
//
// const user = createUser('Gogi', 33);
// console.log(user);
//
// user.increaseAge();
// console.log(user);

/* TASK - 3
* Add some functionality to the previous task
* Write an object method 'addField()', that will be adding a new key for this object.
* Add this method to the resulting object.
* Explanation of the task:
* There will be the new key named 'addField', the value of this key will be the function.
* Function will takes two arguments:
*   1 - the name of new field
*   2 - the value of the new field
* */

// function createUser(userAge, userName) {
//     return {
//         name: userName,
//         age: userAge,
//         increaseAge: function () {
//             this.age++
//         },
//         addField: function (fieldName,fieldValue){
//             this[fieldName] = fieldValue
//         }
//
//     };
// }
// const newUser = createUser(50, 'Oleg');
// newUser.addField('pets', null);
//
// console.log(newUser)

/* TASK - 4
* Rewrite previous task using constructor function.
* Earlier wi was creating an object inside the function
* and this object was the return value of our function.
* But now we must create some kink of an instruction for creating the objects with type User.
* Object with this type will be creating using thw 'new' operator,
* that helps us to call the constructor with the name placed on the right side of the operator.
* */

//Функции - конструкторы

// function User () {
//     this.name = 'Gogi';
//
//     console.log('execution context of the User function - ', this)
// }
// console.log(new User)

//*************************************************************

// function User(name, age) {
//     this.name = name;
//     this.age = age;
//     this.increaseAge = function () {
//         this.age++
//     };
//     this.addField = function (fieldName, fieldValue) {
//         this[fieldName] = fieldValue
//     };
// }
//
// const user = new User('Oleg', 89)
// console.log(user);
// user.increaseAge();
// console.log(user);
// user.addField('pets', []);

/* TASK - 5
* Its a Burger again.
* There is an object with the same basic constants of size and stuffing.
* This object is totally static for us, which means that we can only use the values inside of it.
* */
const constantValues = {
    SIZE_SMALL: {
        price: 15,
        cal: 250,
    },

    SIZE_LARGE: {
        price: 25,
        cal: 340,
    },

    STUFFING_CHEASE: {
        price: 4,
        cal: 25,
    },

    STUFFING_SALAD: {
        price: 5,
        cal: 5,
    },

    STUFFING_BEEF: {
        price: 10,
        cal: 50,
    },
};
/* WHAT WE NEED TO DO:
* Write a constructor function of an object with type Burger, that takes two arguments:
*   1 - the size of the burger
*   2 - the stuffing for the burger
* The arguments can only be string type, and the values of those strings
* can only be the names of the sizes or stuffing inside the constantValues object.
* The resulting object, that will be created using 'new Burger()' construction, must have:
*   1 - size and stuffing keys
*   2 - getPrice() method. Its create a new key called 'price' in current object and calculate the value of the price, depends of size and stuffing options
*   3 - getCalories() method. It does the same thing that getPrice, but calculating calories
* */
function Burger (size, stuffing){
    this.size = constantValues[size];
    this.stuff = constantValues[stuffing];
    this.getPrice = function () {
        this.price = this.size.price + this.stuff.price;
    }
    this.getCalories = function () {
        this.cal = this.size.cal + this.stuff.cal;
    }
}
const myBurger = new Burger ('SIZE_LARGE', 'STUFFING_BEEF');

myBurger.getPrice();
console.log(myBurger.price);
myBurger.getCalories();
console.log(myBurger.cal);

