// function calculate (num1, num2, operation) {
//     switch (operation) {
//         case '+':
//             return num1 + num2;
//         case '-':
//             return num1 - num2;
//         case '*':
//             return num1 * num2;
//         case '/':
//             return num1 / num2;
//         default:
//             return 0;
//     }
// };
//
//
// calculate(2,6,'-');//function call
//
//
// // console.log(calculate); // обращение
// while (confirm('do you want to calc?')){
//     const number = prompt('enter first num');
//     const secondNumber = prompt('enter second num');
//     const  op = prompt('enter operation');
//     calculate(number,secondNumber,op);
// }

//------------------------------------------------------------------------------

//* TASK - 1
// * Create a sum function
// * Arguments: first number, second number
// * Return value: sum of two numbers
// */

// function calcSum (num1, num2) {
//     return num1 + num2
// }
// console.log(calcSum(4,5));

//------------------------------------------------------------------------------
/* TASK - 2
* Write a function that will take two arguments - the number to start the count and the number to finish it.
* By counting we mean the series of numbers in the console with the increase by one.
*/

// function countNumbers (start, end) {
//     for (let i = start; i < end; i++) {
//         console.log(i)
//     }
// console.log('Done');
// }
// countNumbers(0, 200);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

/* TASK - 3
* Write a function that will sum up all the numbers that will be passed to it as arguments.
* */

// function sumAll () {
//     let result = 0;
//     for (let i = 0; i < arguments.length; i++) {
//         result += arguments[i];
//     }
//     return result;
// }
//
// console.log(sumAll(1, 2, 3, 4, 5, 7, 8));
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/* TASK - 4
* Create a function that will check up the number of arguments in it.
* Arguments: three integer numbers
* If the number of arguments is not equal to 3 - display the message:
* "Function called with *ARGUMENTS_LENGTH* arguments!"
* */

// function checkArguments (arg1, arg2, arg3) {
//
//     if (!Number.isInteger(arg1)
//         || !Number.isInteger(arg2)
//         || !Number.isInteger(arg3)) {
//         console.error('one or more arguments are not integer numbers!');
//     }
//     if (arguments.length ==! 3) {
//         console.error(`Function called with ${arguments.length} arguments!`)
//     }
// }
// checkArguments(1,2,3);\
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

/* TASK - 5
* Write the calculateResidue function, which will output ALL numbers in the specified range, which are divisible by the third number.
* Arguments:
*   1) Start point for the range (inclusively)
*   2) End point for the range (inclusively)
*   3) Third number
*   ADVANCED COMPLEXITY:
*     Create a separate function for validation
* */

// function calculateResidue (start, end, divider) {
//         if ( start < 0 && end < 0) {
//             for (let i = start; i >= end ; i--) {
//                 if (i % divider === 0) {
//                     console.log(i);
//                 }
//             }
//         } else if (start > end){
//             for (let i = end; i <= start; i++) {
//                 if (i % divider === 0) {
//                     console.log(i);
//                 }
//             }
//         } else {
//             for (let i = start; i <= end ; i++) {
//                 if (i % divider === 0) {
//                     console.log(i);
//                 }
//             }
//         }
// }
// calculateResidue(8,4,2);
//------------------------------------------------------------------------------

/* TASK - 6
* Write a getPrice() function, that will calculate the cost of the burger, depending on its size and stuffing.
* The size and stuffing are user-defined. User can enter only the names of constants, i.e. there is no need to check for incorrect data entry.
*
* Sizes and stuffings are written in constants:*/
const SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 15,
    cal: 250
};

const SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 25,
    cal: 340
};

const STUFFING_CHEASE = {
    name: 'STUFFING_CHEASE',
    price: 4,
    cal: 25
};

const STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 5,
    cal: 5
};

const STUFFING_BEEF = {
    name: 'STUFFING_BEEF',
    price: 10,
    cal: 50
};

function getPrice (size, stuff) {
    let resultPrice = 0;

    switch (size){
        case 'SIZE_SMALL':
            resultPrice += SIZE_SMALL.price;
            break;
        case 'SIZE_LARGE':
            resultPrice += SIZE_LARGE.price;
            break;
    }
    switch (stuff){
        case 'STUFFING_CHEASE':
            resultPrice += STUFFING_CHEASE.price;
            break;
        case 'STUFFING_SALAD':
            resultPrice += STUFFING_SALAD.price;
            break;
        case 'STUFFING_BEEF' :
            resultPrice += STUFFING_BEEF.price
            break;
    }
    return resultPrice;
}
getPrice('SIZE_LARGE', 'STUFFING_BEEF' );

/* ADVANCED CONDITION - rewrite this code using the Burger constructor function.
* This function must create a copy of the object that contains all the constants at once
* And the getPriceAndCalories method, which calculates the caloric value in addition to the cost
* */
