// const examle = () => {
//     console.log(this)
// }
//
// examle();
//
// const user = {
//     name: 'Gogi',
//     age:55,
//     getBirthDate: function () {
//         console.log('getBirthDate execution context - ',this)
//
//         const dummyFunction = () => {
//             console.log('dummyFunction execution  context',this);
//         };
//
//         function regularHelper () {
//             console.log(('regularHelperExecutionContext - ', this));
//         }
//
//         dummyFunction();
//         regularHelper();
//     }
// };
// user.getBirthDate()
//
// const sum = (a, b) =>  {
//     return a + b
// };
//
// const sum = (a, b) => (a + b);
// const sum = (a, b) => a + b;

/* TASK - 1
* Create sum() function.
* Arguments: two integer numbers
* Return value: sum of two arguments
* */

// const sem = (a,b) => a + b;
//
// function sum (a, b){
//     return a + b
// }
//
// console.log(sem(10, 13));

/* TASK - 2
* Write a functions, that will be counting from start to end of range.
* Arguments: start of the range, and of the range
*/

// const countingFunc = (start, end) => {
//     for (let i = start; i <= end ; i++) {
//         console.log(i);
//     }
// }
// countingFunc(1,15);

/* TASK - 3
* Write a function, that will sum up all the arguments passed into it.
* */

// const sumArg = (...args) => {
//     let sum = 0;
//     for (let key of args) {
//         sum += key;
//     }
//     return sum;
// }
// console.log(sumArg(2, 5, 8, 4));

/* TASK - 4
* Create a function, showMessage(msgText, numberOfShowing)
* Arguments: msg text - is a text of message that will be shown,
* numberOfShowing - the number of times that the message will be shown
* Example: showMessage('Hello', 18) -> will show 'Hello' for the user 18 times
* */

// const showMessage = (msgText, numberOfShowing) => {
//     for (let i = 0; i < numberOfShowing; i++) {
//         console.log(msgText);
//     }
// }
// showMessage('qwerty', 18);

/* TASK - 5
* Create Arrow function, that returns maximum value from all of the arguments that was passed.
* The number of arguments can be any. We need to return the biggest one.
* */

// const maxV = (...args) => {
//     return Math.max(...args)
// }
// console.log(maxV(8232,4235,66,1,2,99999));
// const maxValue = (...args) => Math.max(...args);
// console.log(maxValue(8232,4235,66,1,2,9987));

/* TASK - 6
* Rewrite calculator() function, into the Arrow functions, with a new features:
*
* Each operation has its own function.
* That means - we will have sum(a,b) for summing, multiple(a,b) for multiplying and so on.
* Each of this functions need only two arguments,
* and returns only the result of the operation with this two numbers
* If the function hasn't receive any of two arguments, assign 0 as a default value.
*
* The calculate() functions will be the main one.
* Is takes three arguments:
*   1 - integer number
*   2 - integer number
*   3 - the function needs to be used for this two numbers
* So we have a picture like this:
* the main function inside itself will call the function with the operation.
* This construction is useful, if we need to do some operations with numbers before calculating.
* */
//
// const sumNum = (a, b) => a + b;
// const minusNum = (a, b) => a - b;
// const divideNum = (a, b) => a / b;
// const multipleNum = (a, b) => a * b;
//
// function calculate (firstNum, secondNum, operation) {
//     return operation(firstNum, secondNum);
// }
//
// console.log(calculate(1, 5, sumNum));

const Storage = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    blackTea: 13,
    cheese: 0,
};

const incrementItem = itemName => Storage[itemName]++;
const decrementItem = itemName => Storage[itemName]--;
const resetItem = itemName => Storage[itemName]=0;
const changeItemAmount = (itemName, amount = 0) => Storage[itemName] +=amount;

const changeStorageItem = (itemName,callBack, amount) => {
    callBack(itemName,amount)
}
console.log('apple before changing - ',Storage.apple);
changeStorageItem('apple', changeItemAmount, 5);

console.log('apple after changing - ',Storage.apple);


/* ------------------------------------------------------------------------ */
const randomArrElem = (arr) => {
    const randomIndex = parseInt(Math.random()*arr.length)
    return arr[randomIndex];
}
console.log(randomArrElem([1, 12, {}, 'gogi']));

randomArrElem(Object.entries());