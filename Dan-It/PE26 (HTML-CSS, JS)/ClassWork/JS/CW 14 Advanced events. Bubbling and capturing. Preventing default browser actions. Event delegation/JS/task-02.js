/*
 * Task - 02
 *
 * Create your custom modal window.
 *
 * CSS and markup are placed in html and css files of this classwork.
 *
 * The modal window should appears after click on button "Show modal".
 *
 * Modal window should close after clicking "cross" btn on the top right corner of it, or after clicking anywhere on the screen except the modal itself.
 * */

const openModal = document.createElement('button');
openModal.textContent = 'show open';

openModal.addEventListener('click',e => {
    const divModalWrapper = document.createElement('div');
    divModalWrapper.className = 'modal-wrapper';
    divModalWrapper.insertAdjacentHTML('afterbegin', `<div class="modal">
          <button class="modal-close">x</button>
          <p class="modal-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, enim, repellat! Accusamus deleniti dignissimos distinctio exercitationem, harum ipsa nisi. Aut dignissimos ducimus eveniet in ipsum magnam officiis placeat sunt ullam?</p>
        </div>`);

    divModalWrapper.addEventListener('click', modalEvent => {
     if (modalEvent.target.classList.contains('modal-close')){
       divModalWrapper.remove();
     } else if (modalEvent.target.classList.contains('modal-wrapper')){
         divModalWrapper.remove();
     }
    });

    document.addEventListener('keyup', keyboardEvent => {
        if(keyboardEvent.key.toLowerCase() === 'escape') {
            divModalWrapper.remove();
        }
    });
document.body.prepend(divModalWrapper);
});
document.body.prepend(openModal);

