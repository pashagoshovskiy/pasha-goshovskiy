/*
 * Task - 01
 *
 * Create any text element and container with 5 buttons inside.
 *
 * The task is to show inner text of the button that was just pressed.
 * Only one event listener can be used.
 */

const container = document.createElement('div');
const text = document.createElement('p');

let htmlCode = '<button>1</button>\n<button>2</button>\n<button>3</button>\n<button>4</button>\n<button>5</button>\n';

container.insertAdjacentHTML('afterbegin', htmlCode);
container.prepend(text);

container.addEventListener('click', e => {
    console.log(e.target.tagName);
    if (e.target.tagName === 'BUTTON'){
    let content = e.target.textContent;
    text.textContent = content;
    }
});
document.body.prepend(container);