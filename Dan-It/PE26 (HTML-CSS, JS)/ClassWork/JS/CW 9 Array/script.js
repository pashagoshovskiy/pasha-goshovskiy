// const numbers = [1,2,3,4556,7,333,4,234,45,43,4,3];
//
// console.log(numbers);
//
// numbers.push(55);// добавляет элемент в конец
// numbers.unshift(0);//добавляет элемент в начало, сдвигая все остальные вправо
//
// console.log(numbers);
//
// console.log(numbers.pop());// удаляет элемент с начала массива
//
// console.log(numbers.shift());// удаляет элемент с конца массива
//
// numbers.myForEach = function () {
//     for (let i = 0; i < this.length; i++) {
//         callBack(this[i], i, this);
//     }
// };
//
// numbers.forEach((number, index, array) => {
//
// })

/* TASK - 1
* Create a function, that do not receive any arguments
* it will only ask the user what dishes does he prefer to eat on breakfast.
* User can enter only one dish at one time, so you need to ask him UNTIL he will enter 'end'.
* Each dish needs to be placed into an array.
* Return value: array with all of the dishes.
* */

// const func = () => {
//     let userDish;
//     const allDishes = [];
//     while (userDish !== 'end'){
//         userDish = prompt('what dishes do you prefer to eat on breakfast')
//         allDishes.push(userDish);
//     }
//     allDishes.pop();
//     return allDishes;
// }
//
// console.log(func());;

//ALTERNATIVE
// const breakfast = () => {
//     const allDishes = [];
//     let dish;
//     do {
//         dish = prompt('what dishes does he prefer to eat on breakfast');
//         if (dish !== 'end') {
//             allDishes.push(dish)
//         }
//     } while (dish !== 'end');
//
//     return allDishes
// };
//
// console.log(breakfast());
////////////////////////////////////////////////////////////////////////

/* TASK - 2
* Create a function, that will takes one argument - array from the previous task result.
* The task is to show every dish from the array in console, and also you need to delete it from source array.
* After all of the dishes will be shown on the console, array must be empty.
* */

// const getDishes = () => {
//     let dishesArr = [];
//     let dish;
//
//     do {
//         dish = prompt('Enter your dish ');
//         if (dish !== 'end') {
//             dishesArr.push(dish)
//         }
//     } while (dish !== 'end');
// };
//
// const showDishes = (arr) => {
//     let arrLength = arr.length;
//     for (let i = 0; i < arrLength ; i++) {
//         console.log(arr.shift());
//     }
//     console.log(arr);
// };
// showDishes([1,2,3,4,5,6,7,8,9]);
////////////////////////////////////////////////////////////////////////

/* TASK - 3
* Create a function, that will receive array as an single argument.
* return value: new array, that will be exact copy of the source one.
* Task needs to be done using: for, map(), spread operator. It means three realisations.
*/
// const copyArr = (arr) => {
//     let newArr = [];
//     for (let i = 0; i < arr.length; i++) {
//         newArr.push(arr[i]);
//     }
//     return newArr;
// }
// let numbers = [1,2,3,4,5];
// let cloneNumbers = copyArr(numbers);
//
// console.log(numbers);
// console.log(cloneNumbers);

// const copyArr = (arr) => {
//     return arr.map(addValue => addValue);
// }
// console.log(copyArr([123456, 123454]));

// const copyArr = (arr) => [...arr];
// console.log(copyArr([12345, 123456]));
////////////////////////////////////////////////////////////////////////

/* TASK - 4
* Create function getItemList(), that has to receive string from the user with the names of the items, separated by the ', '.
* Items in the users string can be repeated.
* User string needs to be transformed into an array with unique values.
*/

// const makeUnique = arr => [...new Set(arr)];

// const makeUnique = arr => {
//   const setOutOfArray = new Set(arr);
//   return Array.from(setOutOfArray);
// };

// const makeUnique = arr => {
//     const setOutOfArray = new Set(arr);
//     return [...setOutOfArray];
// };
////////////////////////////////////////////////////////////////////////

/* TASK - 5
* There is a Storage, represented with an array from the previous task.
* User needs to replace one item with one or several values.
*
*
* So we need to write a function replaceItems(insteadOf, insertValue), where:
* insteadOf - the name of the item that needs to be replaced
* insertValue - one or more values that needs to be placed in array
* ITS REQUIRED to check is the insteadOf item presented in the source array.
*/

// const storage = ['apple', 'cheese', 'eggs', 'milk', 'grapes', 'sugar']
// const replaceItems = (insteadOf, insertValue) => {
//     let replaceIndex = storage.indexOf(insteadOf);
//     let insertValues = insertValue.split(', ');
//     let uniqueInsertValues = new Set(insertValues);
//     if(replaceIndex >=0) {
//     storage.splice(replaceIndex, 1, ...uniqueInsertValues);
//     }
// };
// replaceItems('milk', 'gogi, asd, qwe, ert');
// console.log(storage);
////////////////////////////////////////////////////////////////////////

/* TASK - 6
* You have a userList. Each user inside it is represented by an object
* Each user has: fullName, birthDate and array with pets.
* Create a function that receives a userList as an argument
* and shows in console for different arrays:
*   1 - users that have a cats
*   2 - users that have a dogs
*   3 - users that have a dead pets
*   4 - users that have a pet no pets AND are older then 18 years
* */

const USER_LIST = [
    {
        fullName: 'Gogi Doe',
        birthDate: '22/02/1998',
        pets: [
            {
                name: 'Doggidog',
                type: 'dog',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Gogi1 Doe1',
        birthDate: '20/12/1996',
        pets: [
            {
                name: 'Cattycat',
                type: 'cat',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Mister Nowho',
        birthDate: '20/12/1988',
        pets: [
            {
                name: 'Cattycat',
                type: 'iguana',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Missis Nowho',
        birthDate: '20/08/1964',
        pets: [
            {
                name: 'Piter',
                type: 'spider',
                status: 'alive'
            }
        ]
    },
    {
        fullName: 'Helen Shmelen',
        birthDate: '17/08/1999',
        pets: [
            {
                name: 'Princess',
                type: 'peace of devil',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'John Doe',
        birthDate: '19/03/2002',
        pets: [
            {
                name: 'Lord',
                type: 'dog',
                status: 'alive'
            },
            {
                name: 'Gogi',
                type: 'koala',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Georgina Doe',
        birthDate: '06/01/2000',
        pets: [
            {
                name: 'Koko',
                type: 'parrot',
                status: 'alive'
            },
            {
                name: 'Horhe',
                type: 'dog',
                status: 'alive'
            },
            {
                name: 'Piggy',
                type: 'pig',
                status: 'dead'
            }
        ]
    },
    {
        fullName: 'Nonamed Stranger',
        birthDate: '06/01/2000',
        pets: []
    },
    {
        fullName: 'Named Stranger',
        birthDate: '06/01/2000',
        pets: []
    },
];

const petLovers = (petLoversList) => {
    const catLovers = petLoversList.filter(user => {
        const isThereAnyCat = user.pets.some(pet => pet.type === 'cat');
        return isThereAnyCat;
    });
    const dogLovers = petLoversList.filter(user => {
        const isThereAnyDogs = user.pets.some(pet => pet.type === 'dog');
        return isThereAnyDogs;
    });
    const deadPets = petLoversList.filter(user => {
        const isThereAnyDeadPets = user.pets.some(pet => pet.status === 'dead');
        return isThereAnyDeadPets;
    });
    const noPetsAdults = petLoversList.filter(user => {
        const age = 2020 - new Date(user.birthDate).getFullYear();
        return age >= 18 && user.pets.length === 0;
    });
    console.log(catLovers);
    console.log(dogLovers);
    console.log(deadPets);
    console.log(noPetsAdults);
};

