/* TASK - 1
* Create a button 'Click me'.
* By every first click on this button - change background color.
* By every second click - change background color to initial value.
* */

const $button = $('<button class ="button-dec">Click me</button>');

$button.click(() => {
    $button.toggleClass('button-dec-active');
});
$(document.body).prepend($button);