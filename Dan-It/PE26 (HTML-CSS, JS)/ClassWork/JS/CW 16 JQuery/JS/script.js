const $buttons = $('.click-me');

$buttons.css('background', 'teal');

$.fn.gogi = () => {
    console.log("i just added a new method to jQuery")
};

$buttons.gogi();

$buttons.click(function () {
    console.log($(this));
});

console.log($buttons);

$($buttons[2]);
console.log('$buttons.eq(2) - ', $buttons.eq(2));

setTimeout(() => {
    $buttons.eq(2).fadeOut('slow');
}, 3000);

//======== offset ==========

console.log('offset', $buttons.offset());

//====== get the index of the element in its parent ======
const $specialBtn = $('#specialBtn');
console.log('$specialBtn', $specialBtn);
console.log($specialBtn.index());