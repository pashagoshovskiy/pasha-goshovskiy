/* TASK - 3
* Create 'burger' menu.
* Three horizontal lines inside one square element - it gonna be a menu button. DO NOT USE ANY ICONS, ONLY PURE HTML/CSS.
* Add list with any number of items and hide it by default.
* After every first click on menu button - lines needs to become a closing sign('X'), and hidden list needs to be shown.
* After every second click on menu button OR on anywhere outside of list - turn menu button into initial state and hide the list.
* */

const $burgerMenu = $('.burger');
const $menuList = $('#menu-list');
$menuList.hide();
$burgerMenu.click(() => {
    $burgerMenu.toggleClass('burger_toggle')
    $menuList.slideToggle()
   // $menuList.animate({width: 'toggle'}, 5000)
});
