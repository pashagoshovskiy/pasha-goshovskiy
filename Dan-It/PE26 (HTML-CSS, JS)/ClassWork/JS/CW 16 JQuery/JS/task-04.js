/* TASK - 4
* Add "to top" button, that will smoothly scroll the page up to the beginning of the page.
* Button needs to be fixed placed at the bottom right corner of the page and hidden y default.
*
* Show this button only after user scrolled the first screen.
* If user returned to the to of the page - hide the button.
* */

const $btnToTop = $('<button class="to-top-btn">Go up</button>');
$(document.body).prepend($btnToTop);

$btnToTop.click(() => {
$('html, body').animate({scrollTop: 0},1000)
});