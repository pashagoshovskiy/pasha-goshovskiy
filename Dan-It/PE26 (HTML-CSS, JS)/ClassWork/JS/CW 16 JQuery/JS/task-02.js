/* TASK - 2
* Create an input, place it on page.
* If user entered more then 5 symbols inside it - appear the 'Send' button next to input.
* If text length starts to be smaller then 5 again - remove 'Send' button.
* */
const $inputField = $('<input type="text">');
const $sendBtn = $('<button>Send</button>');

$sendBtn.hide();
$(document.body).prepend($inputField, $sendBtn);
$inputField.keyup(() => {
    if ($inputField.val().length > 5) {
        $sendBtn.fadeIn()
    } else {
        $sendBtn.fadeOut()
    }
});