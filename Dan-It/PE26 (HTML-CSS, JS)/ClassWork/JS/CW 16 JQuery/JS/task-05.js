/* TASK - 5
* Create a jQuery function for burger menu creation.
*
* It should work like that:
* $element.burgerMenu({
*   items: ['home', 'portfolio', 'teams', 'contacts'],
* })
*
* You can use the code from this lesson task 3 and just wrap it up with a jQuery function.
* */
$.fn.burgerMenu =function (settings) {
    const burgerBtn = $(`
<div class="burger">
    <div class="burger__line"></div>
    <div class="burger__line"></div>
    <div class="burger__line"></div>
</div>
`);
    const menuItems = settings.items;
    let menuStr = '<ul id="menuList">';
    menuItems.forEach(item => {
        menuStr += `<li>${item}</li>>`;
        });
    menuStr += '</ul>';
    const $menuList = $(menuStr);
    this.append(burgerBtn, $menuList);
};

$('header').burgerMenu({
    items: ['home', 'portfolio', 'teams', 'contacts']
})