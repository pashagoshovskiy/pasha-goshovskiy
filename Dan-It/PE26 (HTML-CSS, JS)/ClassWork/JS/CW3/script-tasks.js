/* TASK - 1
* Get any integer number from user.
* After you get the number, show modal window with message about is number even ot odd.
*/
// let userNumber = +prompt('enter any number!');
/*simple soluition*/
// if (userNumber % 2 === 0) {
//     alert('evenNumber');
// } else {
//     alert('oddNumber');
// }

/*&& - И - логический оператор, И возвращает true, если оба аргумента истинны, а иначе – false.
* || - или - логический оператор, "если хотя бы один из аргументов true, то возвращает true, иначе – false.
* ^ - XOR
* */
// debugger
// const isNumber = !isNaN(userNumber);
//
// if(isNumber) {
//     if (userNumber % 2 === 0) {
//         console.log('Even number');
//     } else {
//         console.log('Odd number');
//     }
// } else {
//     console.log('wrong number');
// }

// switch (userNumber % 2) {
//     case 0:
//         console.log("Even number");
//         break;
//     case 1:
//         console.log("Odd number");
//         break;
//     case -1:
//         console.log("Odd number");
//         break;
//     default:
//         console.log('wrong number');
// }
//-------------------------------------------------------------------------------------------------------

/* TASK - 2
* Ask user in witch language he wants to see the list of the days.
* User can enter only three values, they are - az, en, ru
* Show the list of days on selected language.
* */

// const userLanguage = prompt('Choose your language:ukr, en, ru', 'ukr');

/*Simple*/
//
// if(userLanguage === "ukr"){
//     console.log(("Пн,Вт,Ср,Чт,Пт,Сб,Нд"));
// } else if (userLanguage === "en"){
//     console.log("Mon,Tue,Wed,Th,Fr,Sat,Sun");
// } else if (userLanguage === "ru"){
//     console.log(("Пн,Вт,Ср,Чт,Пт,Сб,Вс"));
// } else {
//     console.log("Choose your language?");
// }

/*Switch*/
// switch (userLanguage) {
//     case 'ukr':
//         console.log(("Пн,Вт,Ср,Чт,Пт,Сб,Нд"));
//         break;
//     case 'ru':
//     console.log(("Пн,Вт,Ср,Чт,Пт,Сб,Вс"));
//         break;
//     case 'en':
//     console.log("Mon,Tue,Wed,Th,Fr,Sat,Sun");
//         break;
//     default:
//     console.log("Choose your language?");
// }

//-------------------------------------------------------------------------------------------------------

/* TASK - 3
* Get the access group name from the user, it can be - 'admin', 'manager' or 'user'.
* And show different message for different access group:
*   - for admin - "Hello, admin!"
*   - for manager - "Hello, manager!"
*   - for user - "Hello, user!"
* */

// const accessGroup = prompt('Enter accessGroup --> admin, manager, user', 'admin');
// console.log('accessGroup =>', accessGroup);

/*if case*/
// if (accessGroup === 'admin'){
//     alert('Hello, admin!');
// } else if (accessGroup === 'manager'){
//     alert('Hello, manager!');
// } else if (accessGroup === 'user'){
//     alert('Hello, user!');
// } else {
//     alert('Bad boy');
// }

/*Switch case*/
// switch (accessGroup) {
//     case 'admin':
//         alert('Hello, admin!');
//         break;
//     case 'manager':
//         alert('Hello, manager!');
//         break;
//     case 'user':
//         alert('Hello, user!');
//         break;
//     default:
//     alert('Bad boy');
// }

//-------------------------------------------------------------------------------------------------------

/* TASK - 4
* We can simulate the staff list with the role for each particular member.
* User enters the name of the employee, after that the message with the role of this employee needs to be shown.
* The list is:
* Boss - the main Boss in the building
* Boss Junior - right hand of the Boss
* John Doe - the worker of the month
* Kicki - schedule writer
* */

// const logIn = prompt('Enter your name!');
/*Switch case*/
// switch (logIn){
//     case 'Boss':
//         alert('The main Boss in the building');
//         break;
//     case 'Boss Junior':
//         alert('Right hand of the Boss');
//         break;
//     case 'John Doe':
//         alert('The worker of the month');
//         break;
//     case 'Kicki':
//         alert('Schedule writer');
//         break;
//     default:
//         alert('Wrong name!')
// }

//-------------------------------------------------------------------------------------------------------

/* TASK - 5
* Write a coffee-machine program.
* Program can accept the coins and prepare the drinks (Coffee 30 UAH, cappuccino 40 UAH, special extra vegan no gluten herbal tea 200 UAH).
* It means that the user enters the amount of money that he gives in the modal window,
* next - he enters the name of the drink he wants.
* Depending of the drink the user was choose, you need to calculate the change and show the message:
* `Your drink *DRINK_NAME* and change *CHANGE*`
* If the change equals to 0, show next message: `Yur drink *DRINK_NAME. Thank you for the exact amount of money!*`
* */
let coffee = 30;
let cappuccino = 40;
let tea = 200;
let userMoney = +prompt('Enter amount of money');
let userDrink = prompt('Enter your drink: coffee, cappuccino, tea');

if (userDrink === "coffee" || userDrink === "cappuccino" || userDrink === "tea") {
    if (userDrink === 'coffee') {
        let change = userMoney - coffee;
        if (userMoney > coffee) {
            alert('Your drink is ' + userDrink + " and change: " + change);
        } else if (userMoney === coffee) {
            alert('Your drink is ' + userDrink);
        } else {
            alert('Not enough money');
        }
    }
    if (userDrink === 'cappuccino') {
        if (userMoney > cappuccino) {
            let change = userMoney - cappuccino;
            alert('Your drink is ' + userDrink + " and change: " + change);
        } else if (userMoney === cappuccino) {
            alert('Your drink is ' + userDrink);
        } else {
            alert('Not enough money');
        }
    }
    if (userDrink === 'tea') {
        if (userMoney > tea) {
            let change = userMoney - tea;
            alert('Your drink is ' + userDrink + " and change: " + change);
        } else if (userMoney === tea) {
            alert('Your drink is ' + userDrink);
        } else {
            alert('Not enough money');
        }
    }
} else {
    alert('Enter correct drink!');
}


