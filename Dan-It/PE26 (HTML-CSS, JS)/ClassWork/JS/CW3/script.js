const thePill = prompt('choose the pill');
// if (thePill === 'red') {
//     console.log('you picked the red one');
// } else if(thePill === 'blue') {
//     console.log('you picked the blue one');
// } else {
//     alert("you didn't pick the pill");
// }
/*USE IT*/
switch (thePill) {
    case "red":
        console.log('you picked the red one');
        break;
    case "blue":
        console.log('you picked the blue one');
        break;
    default:
        alert("you didn't pick the pill");
}

/*alternative (DONT USE IT)*/
switch (true) {
    case thePill === 'red':
        console.log('you picked the red one');
        break;
    case thePill === 'blue':
        console.log('you picked the blue one');
        break;
    default:
        alert("you didn't pick the pill");
}