/* TASK 1
* Show up next things in console, using console.log() method:
  - max integer value
  - min integer value
  - not a number
  - max safe integer
  - min safe integer
* */

console.log('max integer value', Number.MAX_VALUE);
console.log('min integer value', Number.MIN_VALUE);
console.log('Nan integer value', Number.NaN);
console.log('max safe integer', Number.MAX_SAFE_INTEGER);
console.log('min safe integer', Number.MIN_SAFE_INTEGER);
