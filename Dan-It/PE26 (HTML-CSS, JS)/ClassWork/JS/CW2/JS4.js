/* TASK 4
* Execute into console next statements and explain every result in your own words:
* Create variables - x = 6, y = 15, z = 4:
    * x += y  - x++ * z
    * z = --x -y * 6
    * y /= x + 5 % z
* 'random string' + 500
* 'random string' + +'number'
* 'random string' + +'500n'
* 'random string' + parseInt('404not found')
* !!'false' == !!'true'
* 'true' == true
* 'true' === true
* NaN == 1` `NaN == NaN` `NaN === NaN` `NaN > NaN` `NaN < NaN` `NaN >= NaN` `NaN <= NaN
* [] == true` `{} == true
* */

let x = 6;
let y = 15;
let z = 4;
console.log(x += y  - x++ * z);// -3

