/* TASK 3
* Create 2 variables. Assign true for first one, and false for second one.
* Execute into console:
*   - result of comparing 0 and variable with false inside with == operator
*   - result of comparing 1 and variable with true inside with === operator
* */

let a = true;
let b = false;

console.log(b == 0);
console.log(b === 0);
console.log(a === 1);
console.log(a == 1);
