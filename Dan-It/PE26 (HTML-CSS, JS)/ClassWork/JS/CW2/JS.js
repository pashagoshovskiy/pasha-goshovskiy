let num = 33;
let str = 'gogi';
const bool = false;
let obj = {isGogi: false};
const nothing = null;
const hz = undefined;
const unique = Symbol();
let hugeNum = BigInt(555);


console.group("Number");
    console.log('num -',num);
    console.log('num to boolean - ', Boolean(num));
    num = 0;
    console.log('new num -', num)
    console.log('0 to boolean - ', Boolean(num));
console.groupEnd();

console.group("String");
    console.log('str -',str);
    console.log('str to boolean - ', Boolean(str));
    str = '';
    console.log('empty str - ', str);
    console.log('empty string to boolean - ', Boolean(str));
console.groupEnd();

console.group("Boolean");
    console.log('bool -',bool);
    console.log('boolean to boolean - ', Boolean(bool));
console.groupEnd();

console.group("Object");
    console.log('obj -',obj);
    console.log('obj to boolean - ', Boolean(obj));
    obj = {};
    console.log('empty obj -',obj);
    console.log('empty obj to boolean - ', Boolean(obj));
console.groupEnd();

console.group("Nothing(null)");
    console.log('nothing -',nothing);
    console.log('null to boolean - ', Boolean(nothing));
console.groupEnd();

console.group("Undefined");
    console.log('hz -',hz);
    console.log('undefined to boolean - ', Boolean(hz));
console.groupEnd();

console.group("Symbol");
    console.log('unique -',unique);
    console.log('symbol to boolean - ', Boolean(unique));
console.groupEnd();

console.group("BigInt");
    console.log('hugeNum -',hugeNum);
    console.log('BigInt to boolean - ', Boolean(hugeNum));
    hugeNum = 0;
    console.log('0BigInt - ', hugeNum);
    console.log('0BigInt to boolean - ', Boolean(hugeNum));
console.groupEnd();

/* TASK 1
* Show up next things in console, using console.log() method:
  - max integer value
  - min integer value
  - not a number
  - max safe integer
  - min safe integer
* */
console.group('Task-1');
    console.log('max integer value -> ', Number.MAX_VALUE);
    console.log('min integer value -> ', Number.MIN_VALUE);
    console.log('NaN ->', Number.NaN);
    console.log('max safe integer -> ', Number.MAX_SAFE_INTEGER);
    console.log('min safe integer -> ', Number.MIN_SAFE_INTEGER);
console.groupEnd();
/* TASK 2
* Create two variables. First will contain - any number, second - any string with any word inside.
* Execute the SUM of these two variables in the console
* Execute the DEFERENCE between these two variables in the console
* Execute the result of MULTIPLYING these two variables in the console
* Explain the result of every operation by your own words
* */
console.group('Task-2');
    let num1 = 5;
    let str1 = '2';
    console.log('sum -', num1 + str1);
    console.log('minus -', num1 - str1);
    console.log('multiply -', num1 * str1);
console.groupEnd();
/* TASK 3
* Create 2 variables. Assign true for first one, and false for second one.
* Execute into console:
*   - result of comparing 0 and variable with false inside with == operator
*   - result of comparing 1 and variable with true inside with === operator
* */
console.group('Task-3');
let f = false;
let t = true;

console.log('f == 0', f == 0);
console.log('t === 1', t === 1);

console.groupEnd();
/* TASK 4
* Execute into console next statements and explain every result in your own words:
* Create variables - x = 6, y = 15, z = 4:
    * x += y  - x++ * z
    * z = --x -y * 6
    * y /= x + 5 % z
* 'random string' + 500
* 'random string' + +'number'
* 'random string' + +'500n'
* 'random string' + parseInt('404not found')
* !!'false' == !!'true'
* 'true' == true
* 'true' === true
* NaN == 1` `NaN == NaN` `NaN === NaN` `NaN > NaN` `NaN < NaN` `NaN >= NaN` `NaN <= NaN
* [] == true` `{} == true
* */
console.group('Task-4');
let x = 6;
let y = 15;
let z = 4;

console.log("x += y  - x++ * z =>",x += y  - x++ * z); /* x перезаписалось -3*/
console.log("z = --x -y * 6 =>",z = --x -y * 6); /*z перезаписалось на -94, x перезаписалось на -4 (--x)*/
console.log('y /= x + 5 % z =>', y /= x + 5 % z); /*y = 15 */

console.log('"random string" + 500 =>','random string' + 500);
console.log("'random string' + +'number' =>",'random string' + +'number');
console.log("'random string' + +'500n' =>",'random string' + +'500n');
console.log("'random string' + parseInt('404not found') =>",'random string' + parseInt('404not found'));
console.log("!!'false' == !!'true' =>",!!'false' == !!'true');
console.log('true' == true); /*string to number = NaN; boolean to number = 1 , Nan не равно 1 -> false*/
console.log('true' === true); /*=== строгое приведение -> false*/

console.log('NaN == 1', NaN == 1);
console.log('NaN == NaN', NaN == NaN);
console.log('NaN === NaN', NaN === NaN);
console.log('NaN > NaN', NaN > NaN);
console.log('NaN < NaN', NaN < NaN);
console.log('NaN >= NaN', NaN >= NaN);
console.log('NaN <= NaN', NaN <= NaN);
console.log('[] == true', [] == true);
console.log('{} == true', {} == true);
console.groupEnd();

/*Таблица приведения типов данных:
* https://developer.mozilla.org/ru/docs/Web/JavaScript/Equality_comparisons_and_sameness
* */




