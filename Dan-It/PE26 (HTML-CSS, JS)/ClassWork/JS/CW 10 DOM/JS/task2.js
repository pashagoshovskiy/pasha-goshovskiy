/* TASK - 2
* Create a function, that will:
* take an element from the page with class 'training-list', and text content equals 'list-element 5'.
* Show this element in console.
* Replace text content in this element to "<p>Hello</p>" without creating a new HTML element on the page
* Use array methods to complete the task.
* */

const changeText = () => {
   let trainingList = document.querySelector(".training-list")
    console.dir(trainingList);

   for (let elem of trainingList.children){
       if (elem.textContent === 'list-element 5'){
           elem.textContent = '<p>Hello</p>';
           break
       }
   }
}
changeText();
