/* TASK - 1
* Get several elements from the page, by:
*   tag
*   class
*   identifier
*   CSS selector
*   name attribute
* Use console.dir() method to show up the elements
* */

console.dir(document.getElementsByTagName("section"));
console.dir(document.getElementsByClassName('hero-link'));
console.dir(document.getElementById('hero1'));
console.dir(document.querySelectorAll('.hero-text'));
console.dir(document.getElementsByName('unique-link'));