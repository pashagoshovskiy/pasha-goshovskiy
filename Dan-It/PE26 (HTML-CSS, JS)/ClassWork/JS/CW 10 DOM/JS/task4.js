/* TASK - 4
* There is a list of items on the user screen.
* Create a function that will find the 'run out' items, amount of them is equal to 0.
* Replace the 0 inside text content of those elements to 'run out' and change text color to red.
*/

let items = document.getElementsByClassName("storage-item");
// const runOutItems = (allItems) => {
//   let allItemsArr = [...allItems];
//
//   let runOutItemsArr = allItemsArr.filter((item) => {
//     let info = item.textContent.split(' - ');
//     return +info[1] === 0;
//   });
//
//   runOutItemsArr.forEach((item) => {
//     item.style.color = 'red';
//     let info = item.textContent.split(' - ');
//     info[1] = 'run out';
//     item.textContent = info.join(' - ');
//   });
// };

/* ALTERNATIVE WAY */
const runOutItems = (allItems) => {
    let allItemsArr = [...allItems];

    allItemsArr.forEach((item) => {
        let info = item.textContent.split(' - ');
        if (+info[1] === 0) {
            item.style.color = 'red';
            info[1] = 'run out';
            item.textContent = info.join(' - ');
        }
    });
};


runOutItems(items);