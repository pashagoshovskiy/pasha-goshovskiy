/* ЗАДАЧА 3
* Создать гибкую логин форму при помощи функции конструктора
* Аргументы:
*  1 - CSS селектор для поиска родительского элемента для формы
*  2 - объект с настройками для формы. пока что будет содержать только две настройки:
*       - функция валидации логина
*       - функция валидации пароля
*
* Функицонал
*  создаваться форма должна следующим образом
*  new LoginForm('.gogi', {
*   validateLogin: (value) => value,
*   validatePass: (value) => value
*  })
*
* После выполнения этой строки кода на странице внутри указанного селектором элемента должна появится форма
* внутри формы - текстовый инпут для логина, инпут с типом пароль, кнопка сабмит.
* При изменении текстового инпута должна работать функция валидаци логина,
* при изменении инпута с паролем должна работать функция валидаци пароля.
* При непрохождении валидации над инпутом должо показываться сообщение "not valid"
*/

const loginForm = new LoginForm('.form-wrapper', {
    validateLogin: value => value.length > 3 && value.includes('@'),
    validatePass: value => value.length > 8
});

console.log(loginForm.elements.form);

function LoginForm(parentSelector, options) {
    this.elements = {
        form: document.createElement('form'),
        email: document.createElement('input'),
        emailError: document.createElement('span'),
        password: document.createElement('input'),
        passwordError: document.createElement('span'),
        submit: document.createElement('input')
    };

    this.render = () => {
        const {form} = this.elements;

        this.addStylesAndContent();
        this.addEventListeners();

        document.querySelector(parentSelector).append(form);
    };

    this.addEventListeners = () => {
        const {
            email,
            password,
        } = this.elements;

        email.addEventListener('blur', this.handleEmailBlur);
        password.addEventListener('blur', this.handlePasswordBlur);
    };

    this.handleEmailBlur = (event) => {
        const {validateLogin} = options;
        const {emailError} = this.elements;

        const isValid = validateLogin(event.target.value);
        if (!isValid) {
            emailError.textContent = 'not valid';
        } else {
            emailError.textContent = '';
        }
    };

    this.handlePasswordBlur = (event) => {
        const {validatePass} = options;
        const {passwordError} = this.elements;

        const isValid = validatePass(event.target.value);
        if (!isValid) {
            passwordError.textContent = 'not valid';
        } else {
            passwordError.textContent = '';
        }
    };

    this.addStylesAndContent = () => {
        const {
            form,
            email,
            emailError,
            password,
            passwordError,
            submit
        } = this.elements;

        email.type = 'email';
        password.type = 'password';
        submit.type = 'submit';

        email.placeholder = 'Enter email';
        password.placeholder = 'Enter password';
        submit.value = "Login";

        form.className = 'login-form';
        email.className = 'login-form__input';
        emailError.className = 'login-form__error';
        password.className = 'login-form__input';
        passwordError.className = 'login-form__error';
        submit.className = 'login-form__btn';

        form.append(
            emailError,
            email,
            passwordError,
            password,
            submit
        );
    };

    this.render();
}
