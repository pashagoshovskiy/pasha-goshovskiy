/* ЗАДАЧА - 2
* Создать объект пользователя со свойствами имени и возраста.
* Добавить к этому объекту метод getLogin() написанный при помощи стрелочной функции
* внутри метода обращаться к свойствам объекта только через this
* Заставить этот метод работать.
*/

function User(name, age){
    this.age = age;
    this.name = name;
}

console.log(new User('name', 106));
console.log(new User('name', 98));
console.log(new User('name', 98));
console.log(new User('name', 1));