class Machine {
    constructor(name, VIN) {
        this.name = name;
        this.VIN = VIN;
    }

    render() {
        throw new ReferenceError('not implementer yet')
    }
}

class CoffeeMachine extends Machine {
    constructor(name, VIN, {
        selector,
        errorClass,
        drinks,
        supplies
    }) {
        super(name, VIN);

        this.drinks = drinks || [];
        this.supplies = supplies || {
            water: 0,
            milk: 0,
            coffee: 0,
            sugar: 0,
            caneSugar: null
        };
        this.elements = {
            containerSelector: selector,
            form: document.createElement("form"),
            money: document.createElement("input"),
            drinkName: document.createElement("input"),
            submit: document.createElement('button'),
            errorElem: document.querySelector(`.${errorClass}`)
        }
    }

    render() {
        const {form} = elements;

        addStylesAndContent();
        addEventListeners();

        document.querySelector(selector).append(form);
    }
}


const barista5000 = new CoffeeMachine('barista5000', 584574455475, {});

console.log(barista5000);
