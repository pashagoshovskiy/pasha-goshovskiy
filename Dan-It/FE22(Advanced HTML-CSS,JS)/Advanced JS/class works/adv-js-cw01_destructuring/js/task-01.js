/* ЗАДАЧА - 1
* Создать объект пользователя с его данными - имя, фамилия, дата рождения, о себе.
* Создать по переменной для каждого из свойств объекта пользователя.
* Выполнить задание И самым "в лоб" способом, И используя деструктуризацию
*
* ПРОДВИНУТАЯ СЛОЖНОСТЬ: при помощи деструктуризации сохранить значение каждого свойства объекта
* в переменную, имя которой отличается от названия свойства.
* Если искомого свойства нет в объекте - по умолчанию в переменную присваивать пустую строку.
* */
const user = {
    name: 'John',
    surname: 'Doe',
    birthDate: '17.09.1994',
    about: 'JDoe is a good guy'
}
// const name = user.name
// const surname = user.surname
// const birthDate = user.birthDate
// const about = user.about

const {
    name: userName = '',
    surname: userSurname = '',
    birthDate: userBirthDate = '',
    about: userAbout = ''
} = user;


