/* ЗАДАЧА - 4
* Написать функцию, возвращаемым значением которой является объект простой кофемашины.
* Функционал кофемашины:
* 1 - хранит запасы продуктов в свойствах - milk, sugar, water
* 2 - содержит метод getDrinkPrice(drinkName), где аргумент drinkName содержит имя напитка, стоимость которого должен вернуть метод.
* функиця создания кофемашины должна быть объявлена при помощи fuction declration,
* метод getDrinkPrice() - при помощи function expression
*/

const coffeeMachine = {
    milk: 10,
    sugar: 10,
    water: 10,
    drinks: [
        {
            name: 'gogicano', price: 8, milk: 2,
            sugar: 4,
            water: 0
        },
        {name: 'shmogicano', price: 6},
        {name: 'pogicano', price: 11},
        {name: 'matcha tea', price: 100}
    ],
    getDrinkPrice: function (drinkName) {
        const {drinks} = this;

        const {price} = drinks.find(({name}) => name === drinkName);

        return price;
    }
};

coffeeMachine.getDrinkPrice("pogicano"); //expect to return - 11