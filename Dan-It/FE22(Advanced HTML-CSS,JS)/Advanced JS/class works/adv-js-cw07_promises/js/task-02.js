const showItems = () => {
    let messageInput = document.createElement('input');
    let msInput = document.createElement('input');
    let okBtn = document.createElement('button');
    messageInput.type = 'text';
    messageInput.placeholder = 'Enter your message';
    msInput.type = 'number';
    msInput.placeholder = 'Enter delay (ms)';
    okBtn.textContent = 'OK';
    okBtn.addEventListener('click', () => {
        showMessage(messageInput.value, msInput.value);
    })

    document.body.append(messageInput, msInput, okBtn);
}

function showMessage(text, delay) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(text);
        }, delay)
    })
}

showItems();

