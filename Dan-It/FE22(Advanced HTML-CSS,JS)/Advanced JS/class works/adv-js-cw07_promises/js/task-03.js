/* ЗАДАЧА-03
* Написать функцию makeCircle, которая создает и размещает на странице круг.
* Аргументы:
* 1 - диаметр круга, числом без приставки px
* 2 - цвет круга
* 3 - CSS класс, который нужно кругу присвоить
* 4 - время, по истечению которого нужно выполнить следующее дейтсиве
*
* Возвращаемое знаениче: Promise.
*
* Решить задачу нужно так, чтобы после вызова функции makeCircle, можно было вызвать then() и фнукция внутри него сработала через указанное время.
* */
function createBtn() {
    let startBtn = document.createElement('button');

    startBtn.textContent = 'START';

    startBtn.addEventListener('click', () => {
        startBtn.remove();
        makeCircle(250, '#d3d390', 'circle', 3000);
    });
    document.body.prepend(startBtn);
}

function changeBG(elem, newBG, delay) {
    return new Promise((resolve) => {
        setTimeout(() => {
            elem.style.background = newBG;
            resolve(elem);
        }, delay)
    })
}

function makeCircle(diametr, color, classes, delay) {
    let circle = document.createElement('div');
    circle.className = classes;
    circle.style.width = diametr + 'px';
    circle.style.height = diametr + 'px';
    circle.style.backgroundColor = color;

    let colorInput = document.createElement('input');
    colorInput.type = 'color';
    colorInput.placeholder = 'Enter your color';

    let delayInput = document.createElement('input');
    delayInput.type = 'number';
    delayInput.placeholder = 'Enter delay';

    let okBtn = document.createElement('button');
    okBtn.textContent = 'OK';
    okBtn.addEventListener('click', () => {
        changeBG(circle, colorInput.value, delayInput.value);
    });


    return new Promise((resolve, reject) => {
        setTimeout(() => {
            document.body.prepend(circle, colorInput, delayInput, okBtn);

            resolve([circle, colorInput, delayInput, okBtn])
        }, delay)
    })
}

createBtn();
// makeCircle(250, '#d3d390', 'circle', 3000).then(circleElem => {
//     circleElem.style.background = '#f9f';
// });
