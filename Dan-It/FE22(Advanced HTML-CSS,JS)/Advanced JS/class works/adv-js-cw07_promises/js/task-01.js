/* ЗАДАЧА-01
* Написать функцию, которая выводит сообщение на экран.
*
* Аргументы:
* 1 - текст сообщения
* 2 - длительность задержки
*
* Возвращаемое значение: Promise, внутри котрого и вызывается отсрочка.
* */
function showMassage(text, delay) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(text);
            resolve(text)
        }, delay);
    });
}

showMassage('myText', 2000).then((data) => {
    setTimeout(()=>{
        console.log('inside then');
    }, 1000)
})