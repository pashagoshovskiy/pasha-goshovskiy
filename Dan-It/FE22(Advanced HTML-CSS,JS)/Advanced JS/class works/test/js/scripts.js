// const student = {
//     name: 'Joe',
//     age: 16,
//     scores: {
//         maths: 74,
//         english: 63,
//         science: 85
//     }
// };
// const student = {
//     firstname: 'Glad',
//     lastname: 'Chinda',
//     country: 'Nigeria'
// };
//
// const {firstname, lastname, country} = student;


// function displaySummary(student) {
//     console.log('Hello, ' + student.name);
//     console.log('Your Math score is ' + (student.scores.maths || 0));
//     console.log('Your English score is ' + (student.scores.english || 0));
//     console.log('Your Science score is ' + (student.scores.science || 0));
// }

// function displaySummary({name, scores: {maths = 0, english = 0, science = 0}}) {
//     console.log('Hello, ' + student.name);
//     console.log('Your Math score is ' + (student.scores.maths || 0));
//     console.log('Your English score is ' + (student.scores.english || 0));
//     console.log('Your Science score is ' + (student.scores.science || 0));
// }
//
// displaySummary(student);
//
// let country = 'Canada';
// let firstname = 'John';
// let lastname = 'Doe';
//
// const student = {
//     firstname: 'Glad',
//     lastname: 'Chinda',
//     country: 'Nigeria'
// };
//
// ({firstname, lastname} = student);
// console.log(firstname, lastname, country);

const person = {
    name: 'John Doe',
    country: 'Canada'
};

const{name:fullname, country: place, age:years = 25} = person;

console.log(`I am ${fullname} from ${place} and I am ${years} years old.`);



