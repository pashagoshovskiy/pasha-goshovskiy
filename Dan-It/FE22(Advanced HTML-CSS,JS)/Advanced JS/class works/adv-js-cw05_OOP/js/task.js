/* ЗАДАЧА 1
* Написать игру "Угадай число", используя функцию конструктор.
*
* Аргументы:
*   - селектор жлемента, где должен быть размещен инпут
*   - диапазон загадываемого числа в формате объекта: {from: 0, to: 1000}
*
* Вызов метода play() запускает игру.
*
* На странице в указанном месте должен появиться input, куда пользоавтель и будет вводить числа.
*
* За загадывание числа отвечает метод getRandomNumber()
*
* За сравнивание числа отвечает отдельный метод compare(num)
* В качестве аргумента, он получает число, которой пользователь ввел в input
* возвращаемое значение:
*   - если число совпадает - "You win!"
*   - если число введенное пользователем больше загаданного игррой - "You should try a smaller one!"
*   - если число введенное пользователем меньше загаданного игррой - "You should try a bigger one!"
*
* */

function GuessNumber(selector, range) {
    const elements = {
        parent: document.querySelector(selector),
        ok: document.createElement('button'),
        input: document.createElement('input'),
        start: document.createElement('button'),
        message: document.createElement('p')
    };
    this.range = range;
    this.number = null;

    this.getElements = () => elements
}

GuessNumber.prototype.render = function () {
    const {start, parent} = this.getElements();
    start.textContent = 'Start';
    start.addEventListener('click', (e) => this.play(e))
    parent.append(start);
};

GuessNumber.prototype.compare = function (number, guess) {
    debugger
    if (number === guess) {
        return 'You win!'
    } else if (number > guess){
        return 'You should try a bigger one!'
    } else {
        return "You should try a smaller one!"
    }
};

GuessNumber.prototype.handleOk = function (event) {
    console.log(event);
    const {input, message} = this.getElements()
    message.textContent = this.compare(this.number, +input.value)
};

GuessNumber.prototype.getRandomNumber = function () {
    const {from, to} = this.range;

    return Math.floor(Math.random() * (to - from) + from);
};

GuessNumber.prototype.play = function () {
    const {ok, parent, start, input, message} = this.getElements();

    start.remove();

    ok.textContent = "Ok";
    input.placeholder = "Enter number";
    input.type = 'number';

    ok.addEventListener('click', (e) => this.handleOk(e));

    parent.append(input, ok, message)

    this.number = this.getRandomNumber()
};

const game = new GuessNumber('body', {from: 0, to: 1000});
game.render();



