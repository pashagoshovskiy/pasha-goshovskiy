const requestURL = 'https://swapi.dev/api/films/';


fetch(requestURL).then(res => res.json()).then(({results}) => {
    console.log(results)

    results.forEach((i) => {
        const newFilmList = document.createElement('ul');
        document.body.append(newFilmList);
        newFilmList.innerHTML = `<ul><li>${i.title}</li><li>${i.episode_id}</li><li>${i.opening_crawl}</li></ul>`

       i.characters.forEach((personUrl) =>
            fetch(personUrl).then(res => res.json()).then(person => {
                const newPersonsList = document.createElement('p');
                newFilmList.append(newPersonsList)
                newPersonsList.innerHTML = `<li>${person.name}</li>`

            })
        )
    })

})

