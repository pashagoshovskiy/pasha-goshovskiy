const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

class ValidationError extends Error{
    constructor(message) {
        super(`message`);
        this.message = message
    }
}

const root = document.getElementById('root');
const createList = (arr) => {
    arr.forEach(items => {
        // debugger
        if (items.hasOwnProperty('author') && items.hasOwnProperty('name') && items.hasOwnProperty('price')){
            const list = document
                .createElement('ul');
            const { author, name, price } = items;
            list.innerHTML = `<li>${author}</li>
                <li>${name}</li>
                <li>${price}</li>`;
            root.append(list);
        }
        try{
            if(!items.hasOwnProperty('author')) {
                throw new ValidationError('property author is now defined')
            }
            if(!items.hasOwnProperty('name')) {
                throw new ValidationError('property name is now defined')
            }
            if(!items.hasOwnProperty('price')) {
                throw new ValidationError('property price is now defined')
            }
        } catch (error) {
            if(error instanceof ValidationError){

            console.error(`Incorrect data: ${error.message}`)
            }
        }
    })
}
createList(books)