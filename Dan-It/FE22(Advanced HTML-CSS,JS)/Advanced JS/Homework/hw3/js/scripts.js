class Employee {
    constructor(options) {
        this.name = options.name;
        this.age = options.age;
        this.salary = options.salary;
    }

    get age () {
        return this.age * 3
    }
    set age (value) {
        return this.age = value
    }

}

const employee = new Employee({
    name: 'Oleg',
    age: 25,
    salary: 1000
})

class Programmer extends Employee {
    constructor(options) {
        super(options);
        this.lang = options.lang;
    }
    get programmerSalary () {
        return this.salary * 3
    }
}

const programmer = new Programmer({
    lang: ['English', 'Russian']
})