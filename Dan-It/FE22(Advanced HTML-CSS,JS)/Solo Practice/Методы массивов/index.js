const people = [
    {name: 'Dima', age: 20, budget: 40000},
    {name: 'Pasha', age: 22, budget: 50000},
    {name: 'Oleg', age: 17, budget: 2000},
    {name: 'Andrew', age: 25, budget: 1000},
    {name: 'Michael', age: 15, budget: 6500}
]
// ES5
// for (let i = 0; i < people.length; i++ ) {
//     console.log(people[i])
// }
// ES6
// for (let person of people) {
//     console.log(person)
// }

//ForEach
// people.forEach(function (person) {
//     console.log(person)
// })
//ES 5
// people.forEach(person => console.log(person))
// ES 6

//Map
// const newPeople = people.map((person) => person.age * 3
// )
// console.log(newPeople)

//Filter
// const adults = []
// for (let i = 0; i < people.length; i++) {
//     if (people[i].age >= 18) {
//         adults.push(people[i])
//     }
// }
// console.log(adults)
// const adults = people.filter(person => person.age >= 18)
// console.log(adults)

//Reduce
// let amount = 0
// for (let i = 0; i < people.length; i++) {
//     amount += people[i].budget
// }
// const amount = people.reduce((total, person) => total + person.budget,0)
// console.log(amount)

//Find
// const dima = people.find(person => person.name === 'Dima');
// console.log(dima);

//FindIndex
// const dimaIndex = people.findIndex(person => person.name === 'Dima');
// console.log(dimaIndex);


//====================
// const amount = people
//     .filter(person => person.budget > 2500)
//     .map(person => {
//         return{
//             info: `${person.name} (${person.age})`,
//             budget: Math.sqrt(person.budget)
//         }
//     })
//     .reduce((total, person) => total + person.budget, 0)
// console.log(amount);