const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create();

const path = {
    src: {
        scss: './src/scss/**/*.scss',
        html: './index.html'
    },
    build: {
        css: './build/css'
    }
};

/** FUNCTIONS **/
const buildStyles = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(path.build.css))
);

/** WATCHER **/
const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch(path.src.scss, buildStyles).on('change', browserSync.reload);
    gulp.watch(path.src.html).on('change', browserSync.reload);
};

/** TASKS **/
gulp.task('build', gulp.series(
    buildStyles
));

gulp.task('dev', watcher);
