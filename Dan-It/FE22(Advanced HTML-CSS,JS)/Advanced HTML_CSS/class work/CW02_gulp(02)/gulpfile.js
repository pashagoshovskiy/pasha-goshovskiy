const gulp = require('gulp');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const browserSync = require('browser-sync');


const path = {
    src: {
        css: './src/styles/*.css',
        js: './src/scripts/*.js'
    },
    dist: {
        root: './dist/',
        css: './dist/styles/',
        js: './dist/scripts/'
    }
}

/*FUNCTIONS*/

const cleanBuild = () => (
    gulp.src(path.dist.root, {allowEmpty: true})
        .pipe(clean())
)


const buildCss = () => (
    gulp.src(path.src.css)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(path.dist.css))
);

const buildJS = () => (
    gulp.src(path.src.js)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(path.dist.js))
);

const devServer = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    })
}

const watcher = () => {
    devServer();
    gulp.watch(path.src.css, buildCss).on('change', browserSync.reload);
    gulp.watch(path.src.js, buildJS).on('change', browserSync.reload);
}


/*TASKS*/

gulp.task('build', gulp.series(
    cleanBuild,
    buildCss,
    buildJS
));

gulp.task('dev', gulp.series(
    watcher
))