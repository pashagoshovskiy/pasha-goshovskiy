const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    clean = require('gulp-clean');

const path = {
    src: {
        scss: './src/scss/**/*.scss',
        img: './src/img/**/*',
        html: './index.html'
    },
    dist: {
        css: './dist/css/',
        img: './dist/img/',
        root: './dist/'
    }
}

/*FUNCTIONS*/

const createStyle = () => {
    return gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
}
const createImg = () => {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
}
const cleanDist = () => {
    return gulp.src(path.dist.root,{allowEmpty:true})
        .pipe(clean())
}

/*WATCHER*/
const watch = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch(path.src.scss, createStyle).on('change', browserSync.reload);
    gulp.watch(path.src.img, createImg).on('change', browserSync.reload);
    gulp.watch(path.src.html).on('change', browserSync.reload);
}


/*TASKS*/

gulp.task('build', gulp.series(
    cleanDist,
    createStyle,
    createImg
    )
)

gulp.task('dev', watch);