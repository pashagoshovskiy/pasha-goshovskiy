const gulp = require('gulp');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const minify = require('gulp-minify');

const path = {
    build: {
        css:"./build/css/",
        js: "./build/js/",
        fonts: "./build/fonts/",
        img: "./build/img/"
    },
    src: {
        css: './src/style/*.css',
        js: './src/js/*.js',
        fonts: './src/fonts/*.woff2',
        img: './src/img/*'
    }
}

/* F U N C T I O N S*/

const makeStyles = () => {
    return gulp.src(path.src.css).pipe(gulp.dest(path.build.css))
};

const makeClean = () => {
    return gulp.src('./build',{allowEmpty: true}).pipe(clean())
};

const concatJS = () => {
    return gulp.src(path.build.js).pipe(concat('all.js')).pipe(gulp.dest('./build/js/concatJS'))
}

/*T A S K S*/
gulp.task('styles', makeStyles);
gulp.task('clean', makeClean);
gulp.task('concatJS',concatJS);

gulp.task('build', gulp.series(
    makeClean,
    makeStyles
))

const makeJS = () => {
    return gulp.src('./src/js/*.js').pipe(gulp.dest('./build/js/'))
};

gulp.task('moveJS', makeJS);