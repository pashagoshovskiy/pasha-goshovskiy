const gulp = require('gulp');
const concat = require('gulp-concat');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass');

gulp.task('aloha-bro', function () {
    return gulp
        .src([
                './node_modules/bootstrap/dist/css/bootstrap.css',
            ]
        )
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('dist/css'));
});
gulp.task('html:minify', () => {
    return gulp.src('src/index.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
});
gulp.task('t-sass', function () {
    return gulp.src('src/scss/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('dist/css'));
});