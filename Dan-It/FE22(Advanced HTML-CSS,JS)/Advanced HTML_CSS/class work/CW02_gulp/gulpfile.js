const gulp = require('gulp');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();

const path = {
    src: {
        css: './src/styles/*.css',
        js: './src/js/*.js'
    },
    build: {
        root:'./build/',
        css: './build/css/',
        js: './build/js/'
    }
}

/*FUNCTIONS*/

const cleanBuild = () => (
    gulp.src(path.build.root, {allowEmpty:true})
        .pipe(clean())
)

const buildStyles = () => (
    gulp.src(path.src.css)
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream())

);

const buildJS = () => (
    gulp.src(path.src.js)
        .pipe(concat('index.js'))
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream())
)

const devServer = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
};
/*WATCHER*/

const watch = () => {
    devServer();
    gulp.watch(path.src.css, buildStyles).on('change',browserSync.reload);
    gulp.watch(path.src.js, buildJS).on('change',browserSync.reload);
}



/*TASKS*/
gulp.task('makeStyles', buildStyles);
gulp.task('makeJS', buildJS);

/*TASK SERIES*/
gulp.task('build',gulp.series(
   cleanBuild,
    gulp.parallel(
        buildStyles,
        buildJS
    )
));


gulp.task('dev', gulp.series(
    watch
));