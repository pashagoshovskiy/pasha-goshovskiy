import React, {Component, Fragment} from 'react';
import './Modal.scss'
class Modal extends Component {
    render() {
        const {onClick, title, cross, actions,} = this.props
        return (
            <Fragment>
                <div className="modal_overlay" onClick={onClick}/>
                <div className="modal">
                    <h3 className='modal__header'>{title}</h3>
                    {cross && <button onClick={onClick} className='modal__cross'>X</button>}
                    <div className="modal__buttons">
                        {actions}
                    </div>
                </div>

            </Fragment>
        );
    }
}

export default Modal;