import React, {Component} from 'react';
import Card from "../Card/Card";
import './CardList.scss'
import PropTypes from "prop-types";
import App from "../../App";

class CardList extends Component {
    render() {
        const {goods,toggleFavorite,toggleAddToCart} = this.props

        return (
                <div className="cardList">
                    {goods.map(data => {
                        return (
                            <Card toggleFavorite={toggleFavorite} toggleAddToCart={toggleAddToCart} key={data.code} card={data}/>
                        )
                    })}
                </div>
        );
    }
}
CardList.propTypes = {
    toggleFavorite: PropTypes.func,
    toggleAddToCart: PropTypes.func,
    goods: PropTypes.array,
    card: PropTypes.object
}

export default CardList;
