import React from 'react';
import * as Icons from '../../themes/icons'

const Icon = ({type, color, onClick}) => {
    const IconJSX = Icons[type]

    if (!IconJSX) return null

    return (
        <span onClick={onClick}>
            {IconJSX({color})}
        </span>
    );
};

export default Icon;