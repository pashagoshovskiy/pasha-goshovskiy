import React, {Component} from 'react';

class Button extends Component {
    render() {
        const {title,onClick,backgroundColor,className} = this.props
        return (
            <div>
                <button onClick={onClick} style={{backgroundColor:backgroundColor}} className={className}>{title}</button>
            </div>
        );
    }
}

export default Button;