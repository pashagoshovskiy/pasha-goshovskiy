import React, {Component} from 'react';
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import './Card.scss'
import Icon from "../Icon/Icon";
import PropTypes from "prop-types";
import App from "../../App";

class Card extends Component {
    state = {
        isModalOpened: false,
    }
    openModal = () => {
        this.setState({isModalOpened: true})
    }
    closeModal = () => {
        this.setState({isModalOpened: false})
    }


    render() {
        const {card, toggleFavorite, toggleAddToCart} = this.props
        const {isModalOpened} = this.state
        return (
            <div className="card">
                <h1>{card.name}</h1>
                <img src={card.url} alt="card" width="100px" height="100px"/>
                <p>{card.price + "$"}</p>
                <p>{'this is ' + card.color + " vegetable"}</p>
                <Icon type='star' color={card.isFavorite ? 'red' : 'gray'}
                      onClick={() => toggleFavorite(card.code)}/>
                <Button title="Add to card" onClick={this.openModal} backgroundColor='red'/>

                {isModalOpened && <Modal
                    title='Do you want to add this product to the cart?'
                    cross={true}
                    onClick={this.closeModal}
                    actions={[
                        <Button title="Yes"
                                className="modal__button"
                                onClick={() => {
                                    toggleAddToCart(card.code);
                                    this.closeModal();
                                }}
                        />,
                        <Button title="No" className="modal__button" onClick={this.closeModal}/>
                    ]}
                />}
            </div>
        );
    }
}
Card.propTypes = {
    toggleFavorite: PropTypes.func,
    toggleAddToCart: PropTypes.func,
    goods: PropTypes.array,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
    card:PropTypes.object
}

export default Card;

