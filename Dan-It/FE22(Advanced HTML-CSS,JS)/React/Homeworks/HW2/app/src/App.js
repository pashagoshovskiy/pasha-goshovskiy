import React, {Component} from 'react';
import axios from "axios";
import Cardlist from "./Components/Cardlist/Cardlist";
import PropTypes from 'prop-types'

class App extends Component {
    state = {
        goods: []
    }

    normalizeData = (data) => {
        return data.map(el => {
            el.isFavorite = false
            el.addedToCart = false
            return el
        })
    }

    toggleFavorite = (code) => {
        const {goods} = this.state
        const newArray = goods.map(el => {
            if (el.code === code) {
                el.isFavorite = !el.isFavorite
            }
            return el
        })
        this.setState(newArray)
        this.favoriteLocalStorage(code)
    }
    toggleAddToCart = (code) => {
        const {goods} = this.state
        const newArray = goods.map(el => {
            if (el.code === code) {
                el.addedToCart = !el.addedToCart
            }
            return el
        })
        this.setState(newArray)
        this.addToCartLocalStorage(code)
    }
    favoriteLocalStorage = (code) => {
        let array = JSON.parse(localStorage.getItem('favorites')) || []
        array = (array.includes(code)) ? array.filter(el => el !== code) : array.concat(code)
        const favorites = JSON.stringify(array)
        localStorage.setItem('favorites', favorites)
    }
    addToCartLocalStorage = (code) => {
        let array = JSON.parse(localStorage.getItem('addedToCart')) || []
        array = (array.includes(code)) ? array.filter(el => el !== code) : array.concat(code)
        const addedToCart = JSON.stringify(array)
        localStorage.setItem('addedToCart', addedToCart)
    }


    componentDidMount() {
        axios('http://localhost:3000/products.json')
            .then(res => {
                    const normalizeArray = this.normalizeData(res.data)
                    this.setState({goods: normalizeArray})
                }
            )
    }

    render() {
        const {goods} = this.state
        return (
            <div>
                <Cardlist toggleFavorite={this.toggleFavorite} toggleAddToCart={this.toggleAddToCart} goods={goods}/>
            </div>
        );
    }
}
App.propTypes = {
    toggleFavorite: PropTypes.func,
    toggleAddToCart: PropTypes.func,
    goods: PropTypes.array
}

export default App;
