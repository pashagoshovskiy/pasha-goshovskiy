import React, {Component} from 'react';
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";

class App extends Component {
    state = {
        firstButton: {
            title: 'Open first modal',
            bgColor: 'orange'
        },
        secondButton: {
            title: 'Open second modal',
            bgColor: 'green'
        },
        isOpenFirstModal: false,
        isOpenSecondModal: false
    }
    openFirstModal = () => {
        this.setState({isOpenFirstModal: true})
    }
    openSecondModal = () => {
        this.setState({isOpenSecondModal: true})
    }
    closeFirstModal = () => {
        this.setState({isOpenFirstModal: false})
    }
    closeSecondModal = () => {
        this.setState({isOpenSecondModal: false})
    }


    render() {
        const {firstButton, secondButton, modal, isOpenFirstModal, isOpenSecondModal} = this.state
        return (
            <div>
                <Button
                    text={firstButton.title}
                    backgroundColor={firstButton.bgColor}
                    onClick={this.openFirstModal}
                />
                <Button
                    text={secondButton.title}
                    backgroundColor={secondButton.bgColor}
                    onClick={this.openSecondModal}
                    className=""
                />
                {isOpenFirstModal && <Modal
                    header="Do you want to delete this file?"
                    cross={true}
                    text="Once you delete this file, it won’t be possible to undo this action.
                        Are you sure you want to delete it?"
                    onClick={this.closeFirstModal}
                    actions={[
                        <Button text="OK" className='modalButton'/>,
                        <Button text="Exit" className='modalButton' onClick={this.closeFirstModal}/>
                    ]}
                />}
                {isOpenSecondModal && <Modal
                    header="This is Second modal"
                    cross={true}
                    text="Look at the Second modal. Bla Bla Bla Bla Bla Bla Bla Bla"
                    onClick={this.closeSecondModal}
                    actions={[
                        <Button text="OK" className='modalButton'/>,
                        <Button text="Exit" className='modalButton' onClick={this.closeSecondModal}/>
                    ]}
                />}
            </div>
        );
    }
}

export default App;