import React, {Component} from 'react';
import "./Modal.scss"
class Modal extends Component {
    render() {
        const {header, cross, onClick,text, actions} = this.props
        return (
            <div>
                <div className="modal-overlay" onClick={onClick}/>
                <div className="modal">
                    <h3 className="modal__header">{header}</h3>
                    <span>{text}</span>
                    {cross && <button className="crossBtn" onClick={onClick}>X</button>}
                    <div className="modal__buttons">
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;