import React, {Component, Fragment} from 'react';
import './Button.scss'
class Button extends Component {
    render() {
        const {onClick, text, backgroundColor,className} = this.props
        return (
            <Fragment className="modalButtons">
                <button onClick={onClick} style={{backgroundColor: backgroundColor}}
                className={className}>{text}</button>
            </Fragment>
        );
    }
}

export default Button;