import React from 'react'
import ReactDOM from 'react-dom'
// import App from './components/App'

// render(<App/>, document.getElementById('root'))


// ----------------- example 1
// const name = 'Josh perez';
// const element = <h1>Привет {name}</h1>
//
// ReactDOM.render(
//     element,
//     document.getElementById('root')
// )

// // ------- example 2
// function formatName (user) {
//     return user.firstName + '' + user.lastName;
// }
//
// const user = {
//     firstName: 'Harper',
//     lastName: 'Perez'
// };
//
// const element = (
//     <h1>
//         Hello, {getGreeting(user)}
//     </h1>
// )
// function getGreeting (user) {
//     if (user) {
//         return <h1>Привет, {formatName(user)}!</h1>
//     }
//     return <h1> Привет, незнакомец</h1>
// }

//
// ReactDOM.render(
//     element,
//     document.getElementById('root')
// );

// ------------------example 3

// const element = <div tabIndex = '0'></div>
//
// const element2 = <img src={user.avatarUrl}></img>
//
// const element3 = <img src={user.avatarUrl} />
//
// const element4 = (
//     <div>
//     <h1>Hello!</h1>
//     <h2>Nice to see you</h2>
//     </div>
// )
//
// const title = response.potentiallyMaliciousInput;
// const element5 = <h1>{title}</h1>

const element = (
    <h1 className='greeting'>
        Hello world!
    </h1>
)
const element2 = React.createElement(
    'h1',
    {className:'greeting'},
    'Hello world!'
)
const element3 = {
    type: 'h1',
    props: {
        className: 'greeting',
        childrenL:'Hello world!'
    }
};
