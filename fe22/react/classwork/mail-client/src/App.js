import React, {Component} from 'react';
import Header from "./components/Header/Header";
import {Inbox} from "./components/Inbox/Inbox";
import Footer from "./components/Footer/Footer";

const emails = [
    {
        id: 1,
        topic: "Email 1"
    },
    {
        id: 2,
        topic: "Email 2"
    },
    {
        id: 3,
        topic: "Email 3"
    },
    {
        id: 4,
        topic: "Email 4"
    }
]

class App extends Component {
    constructor() {
        super();
    }
    state = {
        title: "Hello world!",
        user: {
            name: "Giya",
            age: 31,
            email: 'email@test.com',
        },
        pagination: 1,
        isLoading: true,
        emails: []
    }


    incrementAge = () => {
        const {user} = this.state
        this.setState({user: {...user, age: user.age + 1}})
    }

    updateName = () => {
        this.setState({user: {...this.state.user, name: "Giya"}})
    }

    updateTitle = () => {
        this.setState({title: "new title"})
    }

    componentDidMount () {
        setTimeout(()=>{
            this.setState({
                emails: emails,
                isLoading: false
            })
        }, 2000)
    }
    componentDidCatch(error, errorInfo) {
        console.log('error',error, errorInfo)
    }

    componentDidUpdate (prevProps,prevState) {
        console.log('componentDidUpdate')
    }
    render()
    {
        const {title, user, emails, isLoading} = this.state

        // if(isLoading) {
        //     return (
        //         <div className='App'>
        //             Loading...
        //         </div>
        //     )
        // }
        return (
            <div className="App">
                <Header
                    title={title}
                    user={user}
                >
                    Some text
                </Header>
                <Inbox
                    emails={emails}
                    updateName={this.updateName}
                    updateTitle={this.updateTitle} incrementAge={this.incrementAge}/>
                <Footer
                    actions={{cancelButton: () => (<button>Cancel button</button>)}}
                    color="blue"/>
            </div>
        );
    }
}

export default App;

