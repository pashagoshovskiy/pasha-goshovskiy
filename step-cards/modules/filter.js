import API from './API.js'

const filter = () => {
    const searchBtn = document.getElementById('search');
    searchBtn.addEventListener('click', function (e) {
        e.preventDefault();
        API.getCards().then(data => {
            const statusDoctor = document.getElementById('status-doctor').value;
            const statusNecessity = document.getElementById('status-necessity').value;
            const visitsArray = data.filter(item => {
                let {doctor, necessity} = item;
                return (statusNecessity === necessity || statusNecessity === 'All')
                    && (statusDoctor === doctor || statusDoctor === 'All')
            })
            console.log(visitsArray);
        })
    })
}
console.log(filter());
export default filter
