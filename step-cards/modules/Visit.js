import Modal from "./Modal.js";
import API from "./API.js";
import filter from "./filter.js";
class Visit {
    constructor() {
        this.modal = new Modal()
    }

    chooseDoctor() {
        const doctorsVariety = new Modal()
        doctorsVariety.show("Choose", "chooseDoc")
        doctorsVariety.addTitle("Choose doctor")
        doctorsVariety.addDropdown(['Cardiologist', 'Dentist', 'Therapist'], 'Doctor', "doctors")
        const chosenDoctor = document.getElementById("doctors")
        document.getElementById("chooseDoc").addEventListener("click", () => {
            if (chosenDoctor.innerText === "Doctor") {
                alert("Choose the doctor, please!")
            } else {
                localStorage.setItem("chosenDoctor", chosenDoctor.innerText)
                doctorsVariety.remove()
            }
        })
    }

    createModal() {
        this.modal.show('Create', "createCard")
        this.modal.addDropdown(['Common', 'Prioritised', 'Immediate'], 'Necessity', "necessity")
        this.modal.addInput('example@domain.com', 'Email', false, "email")
        this.modal.addInput('Name Surname', 'Full Name', false, "fullName")
        this.modal.addTextarea("modal-area form-control", "Describe your problem", "problemDetailed")
    }

    gatherInfo() {
        delete this.modal
        this.necessity = document.getElementById("necessity").innerText
        this.email = document.getElementById("email").value
        this.fullName = document.getElementById("fullName").value
        this.problemDetailed = document.getElementById("problemDetailed").value
    }

    closeModal() {
        document.querySelector(".modal").remove()
    }

    sendCard() {
        API.addCard(this)
    }

    createOnPage() {
        document.querySelectorAll(".card").forEach(card => {
            card.remove()
        })
        filter(allCards => {
            allCards.forEach(item => {
                const cardWrapper = document.createElement("div")
                const card = document.createElement("div")
                const doctorsName = document.createElement("h5")
                const fullName = document.createElement("h6")
                const necessity = document.createElement("p")
                const showMore = document.createElement("a")
                const icons = document.createElement("div")
                const deleteCard = document.createElement("img")
                const editCard = document.createElement("img")

                cardWrapper.className = "card d-inline-flex"
                cardWrapper.id = item.id
                card.classList.add("card-body")
                doctorsName.classList.add("card-title")
                icons.classList.add("d-flex")
                deleteCard.className = "card-title icons"
                deleteCard.src = "img/delete.svg"
                editCard.className = "card-title icons"
                editCard.src = "img/edit.svg"
                showMore.textContent = "Show more..."
                showMore.className = "card-subtitle mb-2 fst-italic text-muted pe-auto"
                showMore.href = ""
                showMore.id = item.id
                fullName.className = "card-subtitle mb-2 text-muted"
                necessity.classList.add("card-text")

                doctorsName.textContent = item.doctor
                fullName.textContent = item.fullName
                necessity.textContent = item.necessity

                deleteCard.addEventListener("click", () => {
                    API.deleteCard(item)
                    document.getElementById(item.id).remove()
                })

                editCard.addEventListener("click", () => {
                    let changeInfo = new Modal()
                    changeInfo.show("Save", "saveCard")
                    changeInfo.addTitle("Change card")
                    for (const [key, value] of Object.entries(item)) {
                        switch (key) {
                            case "necessity":
                                changeInfo.addDropdown(['Common', 'Prioritised', 'Immediate'], 'Necessity', "necessity")
                                break
                            case "email":
                                changeInfo.addInput('example@domain.com', 'Email', false, "email")
                                break
                            case "fullName":
                                changeInfo.addInput('Name Surname', 'Full Name', false, "fullName")
                                break
                            case "problemDetailed":
                                changeInfo.addTextarea("modal-area form-control", "Describe your problem", "problemDetailed")
                                break
                            case "mass":
                                changeInfo.addInput('*KG', 'Body Mass', false, "mass")
                                break
                            case "pressure":
                                changeInfo.addInput('*mmHg', 'Blood Pressure', false, "pressure")
                                break
                            case "age":
                                changeInfo.addInput('*number', 'Your Age', false, "age")
                                break
                            case "previousIllnesses":
                                changeInfo.addTextarea('modal-area form-control', "Any health issues you've experienced previously?", "previousIllnesses")
                                break
                            case "date":
                                changeInfo.addInput('dd-mm-yyy', 'Date of Last Visit', false, "date")
                                break
                        }
                    }
                    document.getElementById("saveCard").addEventListener("click", () => {
                        let allInputs = [document.getElementById("necessity"), ...document.querySelectorAll(".form-control")]
                        let newData = {
                            id: item.id,
                            doctor: item.doctor
                        }
                        allInputs.forEach(input => {
                            for (const [key, value] of Object.entries(item)) {
                                if (input.id === key) {
                                    newData[`${input.id}`] = input.value
                                    if (input.innerText){
                                        newData[`${input.id}`] = input.firstChild.innerHTML
                                    }
                                }
                            }
                        })
                        document.getElementById(`${newData.id}`).remove()
                        API.editCard(newData)
                        changeInfo.remove()
                        location.reload()
                    })
                })

                let additionalInfo = document.createElement("ul")
                additionalInfo.id = item.id
                additionalInfo.className = "list-group d-none"
                for (const [key, value] of Object.entries(item)) {
                    if (key !== "id" && key !== "necessity" && key !== "fullName" && key !== "doctor") {
                        additionalInfo.insertAdjacentHTML("afterbegin", `<li class="list-group-item">${key} : ${value}</li>`)
                    }
                }
                card.append(additionalInfo)

                showMore.addEventListener("click", e => {
                    e.preventDefault()
                    document.querySelectorAll(".list-group").forEach(list => {
                        if (list.id === showMore.id) {
                            list.classList.toggle("d-none")
                        }
                    })
                })

                icons.append(deleteCard, editCard)
                card.prepend(icons, doctorsName, fullName, necessity, showMore)
                cardWrapper.prepend(card)
                document.body.append(cardWrapper)
            })
        })
    }
}

export {
    Visit,
    Modal
}