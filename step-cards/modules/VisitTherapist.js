import {Visit, Modal} from "./Visit.js";

class VisitTherapist extends Visit{
    constructor() {
        super()
        this.doctor = "Therapist"
    }

    createModal() {
        super.createModal();
        localStorage.removeItem("chosenDoctor")
        this.modal.addTitle("Therapist")
    }

}

export default VisitTherapist