const API = {
    cards: 'https://ajax.test-danit.com/api/v2/cards',
    login: 'https://ajax.test-danit.com/api/v2/cards/login',
};

function header(token) {
    return {
        'Content-Type': 'application/json',
        'Authorization': token
            ? `Bearer ${token}`
            : undefined,
    };
}


const auth = (user) => {
    return fetch(API.login, {
        method: 'POST',
        headers: header(localStorage.getItem('token')),
        body: JSON.stringify(user)
    })
};

const getCards = () => {
    return fetch(API.cards, {
        headers: header(localStorage.getItem('token'))
    }).then(r => r.json());
};

const getCard = (cardId) => {
    let certainCard = `https://ajax.test-danit.com/api/v2/cards/${cardId}`
    return fetch(certainCard, {
        headers: header(localStorage.getItem('token'))
    }).then(r => r.json());
};

const addCard = (cardObj) => {
    return fetch(API.cards, {
        method: "POST",
        body: JSON.stringify(cardObj),
        headers: header(localStorage.getItem('token'))
    }).then(r => r.json());
};

const deleteCard = (cardObj) => {
    let certainCard = `https://ajax.test-danit.com/api/v2/cards/${cardObj.id}`
    return fetch(certainCard, {
        method: "DELETE",
        headers: header(localStorage.getItem('token'))
    })
};

const editCard = (cardObj) => {
    let certainCard = `https://ajax.test-danit.com/api/v2/cards/${cardObj.id}`
    return fetch(certainCard, {
        method: "PUT",
        body: JSON.stringify(cardObj),
        headers: header(localStorage.getItem('token'))
    }).then(r => r.json());
};


export default {
    auth,
    getCards,
    getCard,
    addCard,
    deleteCard,
    editCard
}