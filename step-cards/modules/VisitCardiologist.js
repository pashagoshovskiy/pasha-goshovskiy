import {Visit, Modal} from "./Visit.js";

class VisitCardiologist extends Visit{
    constructor() {
        super()
        this.doctor = "Cardiologist"
    }

    createModal() {
        super.createModal();
        localStorage.removeItem("chosenDoctor")
        this.modal.addTitle("Cardiologist")
        this.modal.addInput('*KG' ,'Body Mass', false,"mass")
        this.modal.addInput('*mmHg', 'Blood Pressure', false,"pressure")
        this.modal.addInput('*number', 'Your Age', false,"age")
        this.modal.addTextarea('modal-area form-control', "Any health issues you've experienced previously?","previousIllnesses")
    }
    gatherInfo() {
        super.gatherInfo();
        this.mass = document.getElementById("mass").value
        this.pressure =  document.getElementById("pressure").value
        this.previousIllnesses = document.getElementById("previousIllnesses").value
    }
}

export default VisitCardiologist